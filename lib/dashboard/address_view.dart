import 'dart:async';

import 'package:e_com/anim_btn.dart';
import 'package:e_com/dashboard/add_address.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/styles.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class AddressView extends StatefulWidget {
  const AddressView({Key key}) : super(key: key);

  @override
  _AddressViewState createState() => _AddressViewState();
}

class _AddressViewState extends State<AddressView> {
  final AnimButtonController _animButtonController = AnimButtonController();

  void _doSomething() async {
    _animButtonController.start();
    Timer(const Duration(seconds: 1), () {
      _animButtonController.stop();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: AnimButton(
            title: 'Proceed to Payment',
            width: SizeConfig.widthMultiplier * 80,
            controller: _animButtonController,
            onPressed: _doSomething,
            color: Util.themeColor,
            borderRadius: 18.0,
            height: SizeConfig.heightMultiplier * 6.5,
            successColor: Colors.green,
            curve: Curves.easeIn,
          ),
        ),
        appBar: Util.appBarBlack(
          context,
          'Delivery Address',
        ),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                Util.height(SizeConfig.heightMultiplier * 2),
                Padding(
                  padding: const EdgeInsets.only(left: 22.0),
                  child: Text(
                    'Select delivery address',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.textMultiplier * 2.6),
                  ),
                ),
                Util.height(SizeConfig.heightMultiplier * 3),
                GestureDetector(
                  onTap: () {
                    Util.openActivity(context, const AddAddress());
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.widthMultiplier * 5),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              color: Colors.grey.withOpacity(0.6), width: 1.5)),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 13),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.add_circle_outline_outlined,
                              color: Colors.black,
                              size: 28,
                            ),
                            Util.width(5),
                            Text(
                              'Add New Address',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SizeConfig.textMultiplier * 2.6),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Util.height(SizeConfig.heightMultiplier * 5),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.widthMultiplier * 5),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        border: Border.all(
                            color: Colors.grey.withOpacity(0.6), width: 1.5)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 06, vertical: 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Util.width(10),
                              const Icon(
                                Icons.work_outline_rounded,
                                size: 27,
                              ),
                              Text(
                                ' Work ',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: SizeConfig.textMultiplier * 2.6),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 15, vertical: 8),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Prijam Thapa',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize:
                                          SizeConfig.textMultiplier * 2.6),
                                ),
                                Util.height(5),
                                Text(
                                  '+977 9845672167',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize:
                                          SizeConfig.textMultiplier * 2.0),
                                ),
                                Util.height(10),
                                Text(
                                  'Room #1- Ground Floor, Al Najoum Building, 24 BStreet, Dubai - United Arab Emirates',
                                  style: TextStyle(
                                      color: Colors.black.withOpacity(0.8),
                                      fontWeight: FontWeight.w400,
                                      fontSize:
                                          SizeConfig.textMultiplier * 2.0),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Util.height(SizeConfig.heightMultiplier * 3),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.widthMultiplier * 5),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: AppTheme.textWhiteGrey,
                        border: Border.all(
                            color: Colors.grey.withOpacity(0.3), width: 1.5)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 06, vertical: 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              Util.width(10),
                              const Icon(
                                Icons.home,
                                size: 27,
                                color: Colors.green,
                              ),
                              Expanded(
                                child: Text(
                                  ' Home ',
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                      fontSize:
                                          SizeConfig.textMultiplier * 2.6),
                                ),
                              ),
                              const Icon(
                                Icons.check_circle,
                                color: Colors.green,
                                size: 30,
                              ),
                              Util.width(10),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 15, vertical: 8),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Prijam Thapa',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize:
                                          SizeConfig.textMultiplier * 2.6),
                                ),
                                Util.height(5),
                                Text(
                                  '+977 9845672167',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize:
                                          SizeConfig.textMultiplier * 2.0),
                                ),
                                Util.height(10),
                                Text(
                                  'Room #1- Ground Floor, Al Najoum Building, 24 BStreet, Dubai - United Arab Emirates',
                                  style: TextStyle(
                                      color: Colors.black.withOpacity(0.8),
                                      fontWeight: FontWeight.w400,
                                      fontSize:
                                          SizeConfig.textMultiplier * 2.0),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )));
  }
}
