import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class PromosView extends StatefulWidget {
  const PromosView({Key key}) : super(key: key);

  @override
  _PromosViewState createState() => _PromosViewState();
}

class _PromosViewState extends State<PromosView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Util.appBarBlack(
          context,
          'Promos',
        ),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(context, const SizedBox()));
  }
}
