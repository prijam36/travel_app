import 'package:e_com/dashboard/dash_view.dart';
import 'package:e_com/dashboard/filter_view.dart';
import 'package:e_com/dashboard/my_cart.dart';
import 'package:e_com/notification/notification_view.dart';
import 'package:e_com/profile/setting_view.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class BottomPageHolder extends StatefulWidget {
  const BottomPageHolder({Key key}) : super(key: key);

  @override
  _BottomPageHolderState createState() => _BottomPageHolderState();
}

class _BottomPageHolderState extends State<BottomPageHolder> {
  final PersistentTabController _controller =
      PersistentTabController(initialIndex: 0);

  List<Widget> _navScreens() {
    return [
      const DashView(),
      const FilterView(),
      const MyCartView(),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(Icons.home),
        title: ("Home"),
        activeColorPrimary: CupertinoColors.activeBlue,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.favorite),
        title: ("OFFERS"),
        activeColorPrimary: CupertinoColors.activeBlue,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.person_pin),
        title: ("Help"),
        activeColorPrimary: CupertinoColors.activeBlue,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(),
      body: PersistentTabView(context,
          controller: _controller,
          screens: _navScreens(),
          items: _navBarsItems(),
          confineInSafeArea: true,
          backgroundColor: Colors.white,
          handleAndroidBackButtonPress: true,
          resizeToAvoidBottomInset: true,
          stateManagement: true,
          navBarHeight: MediaQuery.of(context).viewInsets.bottom > 0
              ? 0.0
              : kBottomNavigationBarHeight,
          hideNavigationBarWhenKeyboardShows: true,
          margin: EdgeInsets.all(0.0),
          popActionScreens: PopActionScreensType.all,
          bottomScreenMargin: 0.0, onWillPop: (context) async {
        await showDialog(
          context: context,
          useSafeArea: true,
          builder: (context) => Container(
            height: 50.0,
            width: 50.0,
            color: Colors.white,
            child: ElevatedButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        );
        return false;
      },
          selectedTabScreenContext: (context) {},
          hideNavigationBar: false,
          decoration: NavBarDecoration(

              colorBehindNavBar: Util.themeColor,
              borderRadius: BorderRadius.circular(20.0)),
          popAllScreensOnTapOfSelectedTab: true,
          itemAnimationProperties: const ItemAnimationProperties(
            duration: Duration(milliseconds: 400),
            curve: Curves.ease,
          ),
          screenTransitionAnimation: const ScreenTransitionAnimation(
            animateTabTransition: true,
            curve: Curves.ease,
            duration: Duration(milliseconds: 200),
          ),
          navBarStyle: NavBarStyle.style1),
    );
  }

  Widget drawer() {
    return Drawer(
      child: SafeArea(
        child: Column(
          children: [
            Util.height(SizeConfig.heightMultiplier * 2),
            // userInfo(),
            Util.height(SizeConfig.heightMultiplier * 8),
            Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      GestureDetector(
                        onTap: (){
                          pushNewScreen(
                            context,
                            screen: FilterView(),
                            withNavBar: true, // OPTIONAL VALUE. True by default.
                            pageTransitionAnimation: PageTransitionAnimation.cupertino,
                          );
                        },
                        child: ListTile(
                          leading: Icon(
                            Icons.home,
                            color: Util.themeColor,
                            size: 26,
                          ),
                          title: Transform(
                            transform: Matrix4.translationValues(-12, 0, 0),
                            child: Text(
                              'Home',
                              style: TextStyle(
                                fontSize: SizeConfig.textMultiplier * 2.8,
                                color: Util.themeColor,
                              ),
                            ),
                          ),
                        ),
                      ),
                      ListTile(
                        leading: const Icon(
                          Icons.flag,
                          size: 26,
                        ),
                        title: Transform(
                          transform: Matrix4.translationValues(-12, 0, 0),
                          child: Text(
                            'New Collection',
                            style: TextStyle(
                                fontSize: SizeConfig.textMultiplier * 2.8),
                          ),
                        ),
                      ),
                      ListTile(
                        leading: const Icon(
                          Icons.apps,
                          size: 26,
                        ),
                        title: Transform(
                          transform: Matrix4.translationValues(-12, 0, 0),
                          child: Text(
                            'Categories',
                            style: TextStyle(
                                fontSize: SizeConfig.textMultiplier * 2.8),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          Util.openActivity(context, const NotificationView());
                        },
                        child: ListTile(
                          leading: const Icon(
                            Icons.notifications_active,
                            size: 26,
                          ),
                          title: Transform(
                            transform: Matrix4.translationValues(-12, 0, 0),
                            child: Text(
                              'Notifications',
                              style: TextStyle(
                                  fontSize: SizeConfig.textMultiplier * 2.8),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          Util.openActivity(context, const SettingView());
                        },
                        child: ListTile(
                          leading: const Icon(
                            Icons.settings,
                            size: 26,
                          ),
                          title: Transform(
                            transform: Matrix4.translationValues(-12, 0, 0),
                            child: Text(
                              'Setting',
                              style: TextStyle(
                                  fontSize: SizeConfig.textMultiplier * 2.8),
                            ),
                          ),
                        ),
                      ),
                      ListTile(
                        leading: const Icon(
                          Icons.live_help_rounded,
                          size: 26,
                        ),
                        title: Transform(
                          transform: Matrix4.translationValues(-12, 0, 0),
                          child: Text(
                            'Help',
                            style: TextStyle(
                                fontSize: SizeConfig.textMultiplier * 2.8),
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
            Align(
              alignment: FractionalOffset.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: ListTile(
                  leading: Icon(
                    Icons.exit_to_app,
                    color: Util.themeColor,
                  ),
                  title: InkWell(
                    onTap: () {},
                    child: Padding(
                        padding: const EdgeInsets.only(bottom: 4.0),
                        child: Text(
                          'Logout',
                          style: TextStyle(
                            color: Util.themeColor,
                            fontSize: SizeConfig.textMultiplier * 2.8,
                          ),
                        )),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

}
