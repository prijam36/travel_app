import 'package:e_com/dashboard/messages/chats.dart';
import 'package:e_com/dashboard/messages/notifications.dart';
import 'package:e_com/dashboard/messages/orders.dart';
import 'package:e_com/dashboard/messages/promos.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class MessageView extends StatefulWidget {
  const MessageView({Key key}) : super(key: key);

  @override
  _MessageViewState createState() => _MessageViewState();
}

class _MessageViewState extends State<MessageView> {
  List<Map<String, String>> helperMenu = [
    {'img': 'assets/images/order.png', 'title': 'Orders'},
    {'img': 'assets/images/notification.png', 'title': 'Notification'},
    {'img': 'assets/images/chat.png', 'title': 'Chats'},
    {'img': 'assets/images/promotion.png', 'title': 'Promos'},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Util.appBarNoLeading(context, 'Messages'),
        body: Util().safeArea(
            context,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Util.height(10),
                Row(
                  children: List.generate(
                      helperMenu.length,
                      (index) => Expanded(
                            flex: 2,
                            child: GestureDetector(
                              onTap: () {
                                switch (index) {
                                  case 0:
                                    Util.openActivity(
                                        context, const OrdersView());
                                    break;

                                  case 1:
                                    Util.openActivity(
                                        context, const NotificationView());
                                    break;

                                  case 2:
                                    Util.openActivity(
                                        context, const ChatView());
                                    break;

                                  case 3:
                                    Util.openActivity(
                                        context, const PromosView());
                                    break;

                                  default:
                                    Util().toast(Util.errorMsg, Colors.black);
                                    break;
                                }
                              },
                              child: Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        color: index == 0
                                            ? Colors.blue
                                            : index == 1
                                                ? Colors.yellow[800]
                                                : index == 2
                                                    ? Colors.green
                                                    : Colors.red,
                                        shape: BoxShape.circle),
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Image.asset(
                                        helperMenu[index]['img'],
                                        filterQuality: FilterQuality.high,
                                        height: 22,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  Util.height(10),
                                  Text(helperMenu[index]['title'])
                                ],
                              ),
                            ),
                          )),
                ),
                Util.height(25),
                const Padding(
                  padding: EdgeInsets.only(left: 15.0),
                  child: Text('Last 7 days'),
                ),
                Util.height(10),
                Expanded(
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      itemCount: 10,
                      itemBuilder: (ctx, i) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        color: i % 2 == 0
                                            ? Colors.blue
                                            : Colors.red,
                                        shape: BoxShape.circle),
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Image.asset(
                                        i % 2 == 0
                                            ? helperMenu[0]['img']
                                            : helperMenu[3]['img'],
                                        filterQuality: FilterQuality.high,
                                        height: 22,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  Util.width(10),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          i % 2 != 0
                                              ? "Don't let it go waste"
                                              : 'Sale ends MIDNIGHT',
                                          style: const TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Util.height(5),
                                        const Text(
                                          '18/12/2021',
                                          style: TextStyle(color: Colors.grey),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Util.height(15),
                              i % 2 != 0
                                  ? Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                          color: Colors.grey.withOpacity(0.3),
                                          borderRadius:
                                              BorderRadius.circular(7)),
                                      child: const Padding(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 50),
                                        child: Text(
                                          'Redeem your Rs. 200 welcome gift and save on your fav deals!',
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    )
                                  : Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 2),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(7),
                                        child: Image.network(
                                          'https://previews.123rf.com/images/pattarasin/pattarasin1709/pattarasin170900004/85482188-sale-banner-template-design-big-sale-special-offer-end-of-season-special-offer-banner-vector-illustr.jpg',
                                          filterQuality: FilterQuality.high,
                                          height:
                                              SizeConfig.imageSizeMultiplier *
                                                  40,
                                          width: double.infinity,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    )
                            ],
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider(
                          color: Colors.black.withOpacity(0.3),
                        );
                      },
                    ),
                  ),
                )
              ],
            )));
  }
}
