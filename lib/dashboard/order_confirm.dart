import 'package:e_com/anim_btn.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class OrderComplete extends StatefulWidget {
  const OrderComplete({Key key}) : super(key: key);

  @override
  _OrderCompleteState createState() => _OrderCompleteState();
}

class _OrderCompleteState extends State<OrderComplete> {
  final AnimButtonController _animButtonController = AnimButtonController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(bottom: 20.0),
        child: AnimButton(
          title: 'Continue Shopping',
          width: SizeConfig.widthMultiplier * 80,
          controller: _animButtonController,
          onPressed: () {},
          color: Util.themeColor,
          borderRadius: 18.0,
          height: SizeConfig.heightMultiplier * 6.5,
          successColor: Colors.green,
          curve: Curves.easeIn,
        ),
      ),
      appBar: Util.appBarBlack(context, 'Order Completed'),
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            'assets/images/20943863.jpg',
            filterQuality: FilterQuality.high,
            height: SizeConfig.imageSizeMultiplier * 90,
          ),
          Util.height(20),
          Text(
            ' Order Place Successfully!',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig.textMultiplier * 3.0),
          ),
          Util.height(10),
          Text(
            ' Congratulations! Your order has been placed successfully.',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w400,
                fontSize: SizeConfig.textMultiplier * 2.3),
          ),
        ],
      ),
    );
  }
}
