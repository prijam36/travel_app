import 'dart:async';
import 'package:intl/intl.dart';
import 'package:e_com/anim_btn.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/styles.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class MyCartView extends StatefulWidget {
  const MyCartView({Key key}) : super(key: key);

  @override
  _MyCartViewState createState() => _MyCartViewState();
}

class _MyCartViewState extends State<MyCartView> {
  final AnimButtonController _animButtonController = AnimButtonController();
  var format = NumberFormat.currency(locale: 'HI');
  int totalItem = 0;
  int totalPrice = 0;

  List<Map<String, dynamic>> cartItem = [
    {
      'img': 'assets/images/c1.png',
      'title': 'Niko Organics',
      'detail': 'Size: 7.60 fl oz / 225ml',
      'price': '2145',
      'qty': '3'
    },
    {
      'img': 'assets/images/c2.png',
      'title': 'Men Erek C8120 Running shoe',
      'detail': 'Size: 32 M',
      'price': '3654',
      'qty': '1'
    },
    {
      'img': 'assets/images/c3.png',
      'title': 'Mount Climber Men Jacket',
      'detail': 'Size: XL',
      'price': '7830',
      'qty': '1'
    },
  ];

  void _doSomething() async {
    _animButtonController.start();
    Timer(const Duration(seconds: 1), () {
      _animButtonController.stop();
    });
  }

  void cartPriceQty() {
    List price = [];
    List items = [];
    List totalItems = [];
    for (var element in cartItem) {
      price.add(element['price']);
      items.add(int.parse(element['qty'].toString()));
      totalItem = items.reduce((value, element) => value + element);
      totalItems.add(int.parse(element['qty'].toString()) *
          int.parse(element['price'.toString()]));
      totalPrice = totalItems.reduce((value, element) => value + element);
    }
  }

  void increaseQty(String key, int index) {
    switch (key) {
      case 'add':
        cartItem[index].update(
            'qty', (value) => int.parse(cartItem[index]['qty'].toString()) + 1);
        setState(() {});
        break;
      case 'sub':
        if (cartItem[index]['qty'] != 00) {
          cartItem[index].update('qty',
              (value) => int.parse(cartItem[index]['qty'].toString()) - 1);
        }
        setState(() {});
        break;
      default:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar:
        Padding(
          padding: const EdgeInsets.only(bottom:10.0),
          child: AnimButton(
            title: 'Proceed To Checkout',
            width: SizeConfig.widthMultiplier * 80,
            controller: _animButtonController,
            onPressed: _doSomething,
            color: Util.themeColor,
            borderRadius: 18.0,
            height: SizeConfig.heightMultiplier * 6.5,
            successColor: Colors.green,
            curve: Curves.easeIn,
          ),
        ),
        appBar: Util.appBarBlack(context, 'My Cart'),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            SingleChildScrollView(
              child: Column(
                children: [
                  itemsView(),
                  Util.height(SizeConfig.heightMultiplier * 2),
                  applyPromo(),
                  Util.height(SizeConfig.heightMultiplier * 6),
                  Row(
                    children: [
                      Util.width(30),
                      Expanded(
                        child: Text(
                          'Subtotal',
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2.6,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                        'Rs.' +
                            (format
                                .format(totalPrice)
                                .toString()
                                .replaceAll('INR', '')) +
                            '/-',
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.5,
                            fontWeight: FontWeight.bold),
                      ),
                      Util.width(25),
                    ],
                  ),
                  Util.height(
                    SizeConfig.heightMultiplier * 2.5,
                  ),
                  Row(
                    children: [
                      Util.width(30),
                      Expanded(
                        child: Text(
                          'Deliver Charges',
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2.6,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                        'Rs.' +
                            (format
                                .format((150))
                                .toString()
                                .replaceAll('INR', '')) +
                            '/-',
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.5,
                            fontWeight: FontWeight.bold),
                      ),
                      Util.width(25),
                    ],
                  ),
                  Util.height(SizeConfig.heightMultiplier * 2.5),
                  Row(
                    children: [
                      Util.width(30),
                      Expanded(
                        child: Text(
                          'Order Total',
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2.6,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                        '($totalItem items)',
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 1.8,
                            fontWeight: FontWeight.w500),
                      ),
                      Util.width(10),
                      Text(
                        'Rs.' +
                            (format
                                .format((totalPrice + 150))
                                .toString()
                                .replaceAll('INR', '')) +
                            '/-',
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.5,
                            fontWeight: FontWeight.bold),
                      ),
                      Util.width(25),
                    ],
                  ),

                  Util.height(SizeConfig.heightMultiplier * 3),
                ],
              ),
            )));
  }

  Widget itemsView() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
          children: List.generate(
        cartItem.length,
        (index) {
          cartPriceQty();
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 18),
            child: Row(
              children: [
                Container(
                  padding: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      color: Colors.grey.withOpacity(0.3)),
                  child: Image.asset(
                    cartItem[index]['img'],
                    filterQuality: FilterQuality.high,
                    height: SizeConfig.imageSizeMultiplier * 10,
                    width: SizeConfig.imageSizeMultiplier * 13,
                  ),
                ),
                Util.width(20),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        child: Text(
                          cartItem[index]['title'],
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: SizeConfig.textMultiplier * 2.2),
                        ),
                      ),
                      Util.height(5),
                      Text(
                        cartItem[index]['detail'],
                        style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w500,
                            fontSize: SizeConfig.textMultiplier * 1.8),
                      ),
                      Util.height(10),
                      SizedBox(
                        child: Text(
                          'Rs.' +
                              (format
                                  .format(int.parse(cartItem[index]['price']))
                                  .toString()
                                  .replaceAll('INR', '')) +
                              '/-',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: SizeConfig.textMultiplier * 2.2),
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 5.0),
                      child: IconButton(
                          onPressed: () {
                            cartItem.removeAt(index);
                            if (cartItem.isEmpty) totalPrice = 0;
                            setState(() {});
                          },
                          icon: Center(
                              child: Image.asset(
                            'assets/images/delete.png',
                            filterQuality: FilterQuality.high,
                            color: Colors.red[400],
                            height: SizeConfig.imageSizeMultiplier * 6,
                          ))),
                    ),
                    Row(
                      children: [
                        IconButton(
                            onPressed: () {
                              increaseQty('sub', index);
                            },
                            icon: Center(
                                child: Image.asset(
                              'assets/images/minus.png',
                              filterQuality: FilterQuality.high,
                              color: Colors.red[400],
                              height: SizeConfig.imageSizeMultiplier * 6,
                            ))),
                        Text(
                          int.parse(cartItem[index]['qty'].toString()) < 10
                              ? '0' + cartItem[index]['qty'].toString()
                              : '' + cartItem[index]['qty'].toString(),
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2.2,
                              fontWeight: FontWeight.bold),
                        ),
                        IconButton(
                            onPressed: () {
                              increaseQty('add', index);
                            },
                            icon: Center(
                                child: Image.asset(
                              'assets/images/plus.png',
                              filterQuality: FilterQuality.high,
                              color: Colors.black,
                              height: SizeConfig.imageSizeMultiplier * 6,
                            ))),
                      ],
                    )
                  ],
                ),
                Util.width(10),

                // Row(
                //   children: [
                //     Text(
                //       '${index * 2 + 4}',
                //       style: TextStyle(
                //           color: Colors.black,
                //           fontSize: SizeConfig.textMultiplier * 2.5),
                //     ),
                //     Util.width(10),
                //     const Icon(
                //       Icons.close,
                //       size: 18,
                //     ),
                //     Util.width(10),
                //     Container(
                //       width: SizeConfig.widthMultiplier * 35,
                //       child: Text(
                //         cartItem[index]['title'],
                //         style: TextStyle(
                //             color: Colors.grey,
                //             fontWeight: FontWeight.w500,
                //             fontSize: SizeConfig.textMultiplier * 2.0),
                //       ),
                //     ),
                //   ],
                // ),
                // Util.width(10),
                // Expanded(
                //   child: Text(
                //     'Rs.${index * 210 + 40}',
                //     style: TextStyle(
                //         color: Colors.black,
                //         fontWeight: FontWeight.bold,
                //         fontSize: SizeConfig.textMultiplier * 2.5),
                //   ),
                // ),
              ],
            ),
          );
        },
      )),
    );
  }

  Widget applyPromo() {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: SizeConfig.widthMultiplier * 10),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: AppTheme.textWhiteGrey,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 4),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppTheme.textWhiteGrey,
                      borderRadius: BorderRadius.circular(14.0),
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: SizeConfig.heightMultiplier * 0.42),
                      child: TextFormField(
                        style:
                            TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                        keyboardType: TextInputType.text,
                        onChanged: (val) {},
                        cursorColor: AppTheme.courColor,
                        decoration: const InputDecoration(
                          hintStyle: TextStyle(
                            fontSize: 18,
                          ),
                          hintText: 'Promo Code',
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              TextButton(
                style: TextButton.styleFrom(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    primary: Colors.black,
                    backgroundColor: Colors.black),
                onPressed: () {},
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text(
                    'Apply',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        fontSize: SizeConfig.heightMultiplier * 2),
                  ),
                ),
              ),
              Util.width(25),
            ],
          ),
        ),
      ),
    );
  }

  Widget applyPromo1() {
    return Stack(
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: SizeConfig.widthMultiplier * 10),
          child: Container(
            decoration: BoxDecoration(
              color: AppTheme.textWhiteGrey,
              borderRadius: BorderRadius.circular(14.0),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.heightMultiplier * 0.42),
              child: TextFormField(
                style: TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                keyboardType: TextInputType.text,
                onChanged: (val) {},
                cursorColor: AppTheme.courColor,
                decoration: const InputDecoration(
                  hintStyle: TextStyle(
                    fontSize: 18,
                  ),
                  hintText: '',
                  border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: SizeConfig.widthMultiplier * 18,
          top: 9,
          child: TextButton(
            style: TextButton.styleFrom(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                primary: Colors.black,
                backgroundColor: Colors.black),
            onPressed: () {},
            child: Text(
              'Apply',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w400,
                  fontSize: SizeConfig.heightMultiplier * 2),
            ),
          ),
        )
      ],
    );
  }
}
