import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class CategoryView extends StatefulWidget {
  const CategoryView({Key key}) : super(key: key);

  @override
  _CategoryViewState createState() => _CategoryViewState();
}

class _CategoryViewState extends State<CategoryView> {
  List<Map> category = [
    {'img': 'assets/images/responsive.png', 'title': 'Electronics'},
    {'img': 'assets/images/necklace.png', 'title': 'Jewelery'},
    {'img': 'assets/images/fashion.png', 'title': "Men's clothing"},
    {'img': 'assets/images/dress.png', 'title': "Women's clothing"},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Util.appBarBlack(
          context,
          'All Category',
        ),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            Padding(
                padding: const EdgeInsets.only(left: 25, right: 5),
                child: GridView.count(
                    crossAxisCount: 3,
                    crossAxisSpacing: 10.0,
                    mainAxisSpacing: 10.0,
                    children: List.generate(category.length, (index) {
                      return InkWell(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {},
                        child: Card(
                          elevation: 3.0,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: SizeConfig.widthMultiplier * 5.5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [

                                ClipRRect(
                                  borderRadius: BorderRadius.circular(12.0),
                                  clipBehavior: Clip.antiAlias,
                                  child: Container(
                                    child: Image.asset(
                                      category[index]['img'],
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  category[index]['title'],
                                  textScaleFactor: 1.0,
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.ellipsis,
                                  style: const TextStyle(
                                      fontSize: 12,
                                      fontFamily: 'mb',
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    })))));
  }
}
