import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class WishListView extends StatefulWidget {
  final bool isProfile;

  const WishListView({Key key, this.isProfile = false}) : super(key: key);

  @override
  _WishListViewState createState() => _WishListViewState();
}

class _WishListViewState extends State<WishListView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: widget.isProfile
            ? Util.appBarBlack(context, 'WishList')
            : Util.appBarNoLeading(context, 'WishList'),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            GridView.count(
                physics: const BouncingScrollPhysics(),
                crossAxisCount: 2,
                cacheExtent: 1200,
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 10.0,
                childAspectRatio: 0.75,
                children: List.generate(16, (index) {
                  return InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {},
                    child: Card(
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12)),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.widthMultiplier * 5.5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Stack(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(12.0),
                                  clipBehavior: Clip.antiAlias,
                                  child: Image.network(
                                    index % 3 == 0
                                        ? 'https://ae01.alicdn.com/kf/HTB1BME5kRUSMeJjy1zjq6A0dXXaT.jpg?size=119388&height=800&width=800&hash=989679a63cd1612b47a9ecd90d2df19f'
                                        : 'https://oldnavy.gapcanada.ca/Asset_Archive/ONWeb/content/0028/064/980/assets/211112_15-M6132_M_DP_Sale.jpg',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Positioned(
                                  right: 5.0,
                                  top: 0.0,
                                  child: Container(
                                    width: 35,
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.white),
                                    child: IconButton(
                                      splashColor: Colors.transparent,
                                      hoverColor: Colors.transparent,
                                      onPressed: () {},
                                      icon: Icon(
                                        index % 3 == 0
                                            ? Icons.favorite
                                            : Icons.favorite_border,
                                        size: 16,
                                        color: index % 3 == 0
                                            ? Colors.red
                                            : Colors.black,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              index % 3 == 0
                                  ? 'Women Cotton Long-sleeve'
                                  : 'Men Cotton Winter Collection',
                              textScaleFactor: 1.0,
                              maxLines: 2,
                              textAlign: TextAlign.start,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                  fontSize: 12,
                                  fontFamily: 'mb',
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400),
                            ),
                            Util.height(8),
                            SizedBox(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    'Rs.28,00',
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 19),
                                  ),
                                  Util.height(2),
                                  const Text(
                                    'Rs.3200000,00',
                                    style: TextStyle(
                                        decoration: TextDecoration.lineThrough,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 17),
                                  ),
                                ],
                              ),
                            ),
                            Util.height(5),
                            RatingBar.builder(
                              initialRating: 4,
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              unratedColor: Colors.grey.withOpacity(0.6),
                              itemCount: 5,
                              itemSize: 14.0,
                              itemPadding:
                                  const EdgeInsets.symmetric(horizontal: 1.0),
                              itemBuilder: (context, _) => const Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {},
                              updateOnDrag: true,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }))));
  }
}
