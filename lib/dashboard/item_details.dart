import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class ItemDetails extends StatefulWidget {
  const ItemDetails({Key key}) : super(key: key);

  @override
  _ItemDetailsState createState() => _ItemDetailsState();
}

class _ItemDetailsState extends State<ItemDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Util.appBarBlack(context, 'Checkout'),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                Stack(
                  children: [
                    Image.network(
                      'https://ae01.alicdn.com/kf/HTB1BME5kRUSMeJjy1zjq6A0dXXaT.jpg?size=119388&height=800&width=800&hash=989679a63cd1612b47a9ecd90d2df19f',
                      filterQuality: FilterQuality.high,
                      errorBuilder: (BuildContext context, Object exception,
                          StackTrace stackTrace) {
                        return Image.asset(
                          "assets/images/no_img.png",
                          filterQuality: FilterQuality.high,
                          fit: BoxFit.cover,
                          width: 100,
                          height: 200,
                        );
                      },
                    ),
                    Positioned(
                      right: 10,
                      top: 10,
                      child: Container(
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle, color: Colors.white),
                        child: IconButton(
                          splashColor: Colors.transparent,
                          hoverColor: Colors.transparent,
                          onPressed: () {},
                          icon: const Icon(
                            Icons.favorite,
                            size: 28,
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            )));
  }
}
