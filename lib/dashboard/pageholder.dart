import 'package:e_com/auth/login_view.dart';
import 'package:e_com/dashboard/category_view.dart';
import 'package:e_com/dashboard/dash_view.dart';
import 'package:e_com/dashboard/messages_view.dart';
import 'package:e_com/dashboard/order_confirm.dart';
import 'package:e_com/dashboard/wish_list.dart';
import 'package:e_com/notification/notification_view.dart';
import 'package:e_com/profile/setting_view.dart';
import 'package:e_com/provider/user_auth.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:titled_navigation_bar/titled_navigation_bar.dart';

class PageHolder extends StatefulWidget {
  const PageHolder({Key key}) : super(key: key);

  @override
  _PageHolderState createState() => _PageHolderState();
}

class _PageHolderState extends State<PageHolder> {
  static final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  int currentIndex = 0;
  Map userData;
  final List<TitledNavigationBarItem> items = [
    TitledNavigationBarItem(title: const Text('Home'), icon: Icons.home),
    TitledNavigationBarItem(
        title: const Text('Message'), icon: Icons.message_outlined),
    TitledNavigationBarItem(
        title: const Text('Wishlist'), icon: Icons.favorite),
    TitledNavigationBarItem(title: const Text('Setting'), icon: Icons.settings),
  ];
  var currentTab = [
    DashView(
      drawerOpen: () {
        _scaffoldKey.currentState.openDrawer();
      },
    ),
    const MessageView(),
    const  WishListView(
      isProfile: false,
    ),
    const SettingView(
      isProfile: false,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<BottomNavigationBarProvider>(context);
    final data = context.watch<UserService>().getUser();
    return Scaffold(
      key: _scaffoldKey,
      drawer: drawer(data),
      body: currentTab[provider.currentIndex],
      bottomNavigationBar: TitledBottomNavigationBar(
        onTap: (index) {
          provider.currentIndex = index;
          currentIndex = index;
        },
        curve: Curves.easeInBack,
        items: items,
        activeColor: Colors.red,
        currentIndex: currentIndex,
        inactiveColor: Colors.blueGrey,
      ),
    );
  }

  Widget drawer(Map user) {
    return Drawer(
      child: SafeArea(
        child: Column(
          children: [
            Util.height(SizeConfig.heightMultiplier * 2),
            userInfo(user),
            Util.height(SizeConfig.heightMultiplier * 12),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: ListView(
                physics: const BouncingScrollPhysics(),
                shrinkWrap: true,
                children: [
                  GestureDetector(
                    onTap: () {
                      Util.closeActivity(context);
                    },
                    child: ListTile(
                      leading: Icon(
                        Icons.home,
                        color: Util.themeColor,
                        size: 26,
                      ),
                      title: Transform(
                        transform: Matrix4.translationValues(-12, 0, 0),
                        child: Text(
                          'Home',
                          style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.8,
                            color: Util.themeColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Util.openActivity(context, const OrderComplete());
                    },
                    child: ListTile(
                      leading: const Icon(
                        Icons.flag,
                        size: 26,
                      ),
                      title: Transform(
                        transform: Matrix4.translationValues(-12, 0, 0),
                        child: Text(
                          'New Collection',
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2.8),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Util.openActivity(context, const CategoryView());
                    },
                    child: ListTile(
                      leading: const Icon(
                        Icons.apps,
                        size: 26,
                      ),
                      title: Transform(
                        transform: Matrix4.translationValues(-12, 0, 0),
                        child: Text(
                          'Categories',
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2.8),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Util.openActivity(context, const NotificationView());
                    },
                    child: ListTile(
                      leading: const Icon(
                        Icons.notifications_active,
                        size: 26,
                      ),
                      title: Transform(
                        transform: Matrix4.translationValues(-12, 0, 0),
                        child: Text(
                          'Notifications',
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2.8),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Util.openActivity(context, const SettingView(
                        isProfile: true,
                      ));
                    },
                    child: ListTile(
                      leading: const Icon(
                        Icons.settings,
                        size: 26,
                      ),
                      title: Transform(
                        transform: Matrix4.translationValues(-12, 0, 0),
                        child: Text(
                          'Setting',
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2.8),
                        ),
                      ),
                    ),
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.live_help_rounded,
                      size: 26,
                    ),
                    title: Transform(
                      transform: Matrix4.translationValues(-12, 0, 0),
                      child: Text(
                        'Help',
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.8),
                      ),
                    ),
                  ),
                ],
              ),
            )),
            Align(
              alignment: FractionalOffset.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: ListTile(
                  leading: Icon(
                    Icons.exit_to_app,
                    color: Util.themeColor,
                  ),
                  title: InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      _openCustomDialog();
                    },
                    child: Padding(
                        padding: const EdgeInsets.only(bottom: 4.0, top: 5),
                        child: Text(
                          'Logout',
                          style: TextStyle(
                            color: Util.themeColor,
                            fontSize: SizeConfig.textMultiplier * 2.8,
                          ),
                        )),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  Widget userInfo(Map user) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Hero(
            tag: 'pp',
            child: CircleAvatar(
              radius: 50.0,
              backgroundImage: NetworkImage(
                  'https://images.fineartamerica.com/images/artworkimages/mediumlarge/2/zoro-one-piece-anime-ihab-design.jpg'),
              backgroundColor: Colors.transparent,
            ),
          ),
          Expanded(
            child: ListTile(
              title: Text(
                'Prijam Thapa',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: SizeConfig.textMultiplier * 3.5,
                    color: Colors.black),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(
                  user['email'],
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.grey,
                      fontSize: SizeConfig.textMultiplier * 2.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _openCustomDialog() {
    showGeneralDialog(
        barrierColor: Colors.transparent,
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
              opacity: a1.value,
              child: AlertDialog(
                title: const Text(
                  'Confirm Logout',
                  textScaleFactor: 1.0,
                ),
                content: const Text(
                  'Are you sure you want to logout?',
                  textScaleFactor: 1.0,
                ),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(
                      'Cancel',
                      textScaleFactor: 1.0,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  TextButton(
                    onPressed: () async {
                      Util.replaceActivity(context, const LoginView());
                    },
                    child: const Text(
                      'Yes',
                      textScaleFactor: 1.0,
                      style: TextStyle(color: Colors.red),
                    ),
                  )
                ],
              ),
            ),
          );
        },
        transitionDuration: const Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        });
  }
}



class BottomNavigationBarProvider with ChangeNotifier {
  int _currentIndex = 0;

  get currentIndex => _currentIndex;

  set currentIndex(int index) {
    _currentIndex = index;
    notifyListeners();
  }
}
