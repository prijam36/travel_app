import 'dart:async';

import 'package:e_com/anim_btn.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;

class FilterView extends StatefulWidget {
  const FilterView({Key key}) : super(key: key);

  @override
  _FilterViewState createState() => _FilterViewState();
}

class _FilterViewState extends State<FilterView> {
  double _lowerValue = 20.0;
  double _upperValue = 80.0;
  List selectedData = [];
  List selectedSize = [];
  List selectedColor = [];
  final AnimButtonController _animButtonController = AnimButtonController();

  List<Map> brands = [
    {'name': 'Zara', 'isSelected': false},
    {'name': 'Nike', 'isSelected': false},
    {'name': 'Erek', 'isSelected': false},
  ];

  List<Map> sizes = [
    {'name': 'XS', 'isSelected': false},
    {'name': 'M', 'isSelected': false},
    {'name': 'L', 'isSelected': false},
    {'name': 'XL', 'isSelected': false},
    {'name': '2XL', 'isSelected': false},
  ];
  List<Map> colors = [
    {'name': Colors.red, 'isSelected': false},
    {'name': Colors.black, 'isSelected': false},
    {'name': Colors.blueGrey, 'isSelected': false},
    {'name': Colors.blue, 'isSelected': false},
    {'name': Colors.deepPurple, 'isSelected': false},
  ];

  void _doSomething() async {
    _animButtonController.start();
    Timer(const Duration(seconds: 1), () {
      _animButtonController.stop();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Util.appBarBlack(context, 'Filter'),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            Padding(
              padding: const EdgeInsets.only(left: 25, right: 5),
              child: ListView(
                children: [
                  Util.height(10),
                  Text(
                    'PRICE RANGE',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.textMultiplier * 2.2),
                  ),
                  Util.height(SizeConfig.heightMultiplier * 3.5),
                  frs.RangeSlider(
                    min: 0.0,
                    max: 150.0,
                    lowerValue: _lowerValue,
                    upperValue: _upperValue,
                    divisions: 100,
                    showValueIndicator: true,
                    valueIndicatorMaxDecimals: 0,
                    onChanged: (double newLowerValue, double newUpperValue) {
                      setState(() {
                        _lowerValue = newLowerValue;
                        _upperValue = newUpperValue;
                      });
                    },
                    onChangeStart:
                        (double startLowerValue, double startUpperValue) {
                      print(
                          'Started with values: $startLowerValue and $startUpperValue');
                    },
                    onChangeEnd: (double newLowerValue, double newUpperValue) {
                      print(
                          'Ended with values: $newLowerValue and $newUpperValue');
                    },
                  ),
                  Util.height(SizeConfig.heightMultiplier * 1.5),
                  ListTile(
                    leading: Transform(
                      transform: Matrix4.translationValues(-16, 0, 0),
                      child: Text(
                        'BRANDS',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: SizeConfig.textMultiplier * 2.2),
                      ),
                    ),
                    trailing: GestureDetector(
                      onTap: () {
                        selectedData.clear();
                        for (int i = 0; i < brands.length; i++) {
                          selectedData.add(brands[i]);
                        }
                        setState(() {});
                      },
                      child: Transform(
                        transform: Matrix4.translationValues(0, -5, 0),
                        child: Text(
                          'See All',
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.w500,
                              fontSize: SizeConfig.textMultiplier * 2.0),
                        ),
                      ),
                    ),
                  ),
                  Util.height(SizeConfig.heightMultiplier * 0.8),
                  Column(
                    children: List.generate(brands.length, (index) {
                      return GestureDetector(
                        onTap: () {
                          if (selectedData.contains(index)) {
                            selectedData.remove(index);
                          } else {
                            selectedData.add(index);
                          }
                          setState(() {});
                        },
                        child: Column(
                          children: [
                            ListTile(
                              leading: Transform(
                                transform: Matrix4.translationValues(-11, 0, 0),
                                child: Text(
                                  brands[index]['name'],
                                  style: TextStyle(
                                      color: selectedData.contains(index)
                                          ? const Color.fromRGBO(
                                              254, 92, 69, 1.0)
                                          : Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize:
                                          SizeConfig.textMultiplier * 2.2),
                                ),
                              ),
                              trailing: !selectedData.contains(index)
                                  ? const SizedBox()
                                  : Transform(
                                      transform:
                                          Matrix4.translationValues(0, -5, 0),
                                      child: Icon(
                                        Icons.check,
                                        color: selectedData.contains(index)
                                            ? const Color.fromRGBO(
                                                254, 92, 69, 1.0)
                                            : Colors.black,
                                      )),
                            ),
                            const Padding(
                              padding: EdgeInsets.only(right: 8.0),
                              child: Divider(
                                thickness: 1.2,
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
                  ),
                  Util.height(SizeConfig.heightMultiplier * 2.8),
                  Text(
                    'SIZES',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.textMultiplier * 2.2),
                  ),
                  Util.height(SizeConfig.heightMultiplier * 1.8),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.start,
                    spacing: 20.0,
                    children: List.generate(sizes.length, (index) {
                      return GestureDetector(
                          onTap: () {
                            if (selectedSize.contains(index)) {
                              selectedSize.remove(index);
                            } else {
                              selectedSize.add(index);
                            }
                            setState(() {});
                          },
                          child: Container(
                            width: SizeConfig.widthMultiplier * 13,
                            height: SizeConfig.heightMultiplier * 7,
                            child: Center(
                              child: Text(
                                sizes[index]['name'],
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: SizeConfig.textMultiplier * 2.0,
                                    color: Colors.white),
                              ),
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: selectedSize.contains(index)
                                    ? const Color.fromRGBO(254, 92, 69, 1.0)
                                    : Colors.grey),
                          ));
                    }),
                  ),
                  Util.height(SizeConfig.heightMultiplier * 2.8),
                  Text(
                    'COLORS',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.textMultiplier * 2.2),
                  ),
                  Util.height(SizeConfig.heightMultiplier * 1.8),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.start,
                    spacing: 20.0,
                    children: List.generate(colors.length, (index) {
                      return GestureDetector(
                          onTap: () {
                            if (selectedColor.contains(index)) {
                              selectedColor.remove(index);
                            } else {
                              selectedColor.add(index);
                            }
                            setState(() {});
                          },
                          child: Stack(
                            children: [
                              Container(
                                width: SizeConfig.widthMultiplier * 13,
                                height: SizeConfig.heightMultiplier * 7,
                                child: Center(
                                  child: Text(
                                    '',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize:
                                            SizeConfig.textMultiplier * 2.0,
                                        color: Colors.white),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: colors[index]['name']),
                              ),
                              selectedColor.contains(index)
                                  ? Positioned(
                                      top: SizeConfig.heightMultiplier * 2,
                                      left: SizeConfig.widthMultiplier * 3.5,
                                      child: const Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 23,
                                      ))
                                  : const SizedBox()
                            ],
                          ));
                    }),
                  ),
                  Util.height(SizeConfig.heightMultiplier * 6),
                  AnimButton(
                    title: 'Apply',
                    width: SizeConfig.widthMultiplier * 80,
                    controller: _animButtonController,
                    onPressed: _doSomething,
                    color: Util.themeColor,
                    borderRadius: 18.0,
                    height: SizeConfig.heightMultiplier * 6.5,
                    successColor: Colors.green,
                    curve: Curves.easeIn,
                  ),
                  Util.height(SizeConfig.heightMultiplier * 2.8),

                ],
              ),
            )));
  }
}
