import 'package:carousel_slider/carousel_slider.dart';
import 'package:e_com/size_config.dart';
import 'package:flutter/material.dart';

class SliderView extends StatefulWidget {
  const SliderView({Key key}) : super(key: key);

  @override
  State<SliderView> createState() => _SliderViewState();
}

class _SliderViewState extends State<SliderView> {
  final List<String> imageList = [
    "https://cdn.pixabay.com/photo/2017/12/03/18/04/christmas-balls-2995437_960_720.jpg",
    "https://cdn.pixabay.com/photo/2017/12/13/00/23/christmas-3015776_960_720.jpg",
    "https://cdn.pixabay.com/photo/2019/12/19/10/55/christmas-market-4705877_960_720.jpg",
    "https://cdn.pixabay.com/photo/2019/12/20/00/03/road-4707345_960_720.jpg",
    "https://cdn.pixabay.com/photo/2016/11/22/07/09/spruce-1848543__340.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        autoPlay: true,
        aspectRatio: 2.0,
        enlargeCenterPage: true,
      ),
      items: imageList
          .map((item) => SizedBox(
                height: 150,
                child: Center(
                    child: ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: Image.network(
                    item,
                    width: double.infinity,
                    fit: BoxFit.cover,
                    height: SizeConfig.heightMultiplier * 20,
                  ),
                )),
              ))
          .toList(),
    );
  }
}
