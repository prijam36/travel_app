import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class SearchView extends StatefulWidget {
  const SearchView({Key key}) : super(key: key);

  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  final TextEditingController _searchQueryController = TextEditingController();
  String searchQuery = "Search query";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Util.closeActivity(context);
            },
            icon: Container(
              decoration: const BoxDecoration(
                  color: Colors.black, shape: BoxShape.circle),
              child: const Padding(
                padding: EdgeInsets.all(4.0),
                child: Icon(
                  Icons.keyboard_arrow_left_sharp,
                  color: Colors.white,
                  size: 25,
                ),
              ),
            ),
          ),
          title: _buildSearchField(),

        ),
        body: Util().safeArea(context, SizedBox()));
  }

  Widget _buildSearchField() {
    return TextField(
      controller: _searchQueryController,
      cursorColor: Util.themeColor,
      autofocus: true,
      decoration: const InputDecoration(
        hintText: "Search",

        border: InputBorder.none,
        hintStyle: TextStyle(color: Colors.black),
      ),
      style: const TextStyle(color: Colors.black, fontSize: 19.0),

    );
  }


}
