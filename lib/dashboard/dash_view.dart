import 'package:e_com/auth/login_view.dart';
import 'package:e_com/dashboard/category_view.dart';
import 'package:e_com/dashboard/filter_view.dart';
import 'package:e_com/dashboard/item_details.dart';
import 'package:e_com/dashboard/my_cart.dart';
import 'package:e_com/dashboard/order_confirm.dart';
import 'package:e_com/dashboard/search_view.dart';
import 'package:e_com/notification/notification_view.dart';
import 'package:e_com/profile/setting_view.dart';
import 'package:e_com/provider/user_auth.dart';
import 'package:e_com/size_config.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class DashView extends StatefulWidget {
  final Function drawerOpen;

  const DashView({Key key, this.drawerOpen}) : super(key: key);

  @override
  State<DashView> createState() => _DashViewState();
}

class _DashViewState extends State<DashView> {
  static final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  List<Color> banner = [Colors.deepOrange, Colors.blue, Colors.purpleAccent];
  PageController controller;

  final List<String> imageList = [
    "https://cdn.pixabay.com/photo/2017/12/03/18/04/christmas-balls-2995437_960_720.jpg",
    "https://cdn.pixabay.com/photo/2017/12/13/00/23/christmas-3015776_960_720.jpg",
    "https://cdn.pixabay.com/photo/2019/12/19/10/55/christmas-market-4705877_960_720.jpg",
    "https://cdn.pixabay.com/photo/2019/12/20/00/03/road-4707345_960_720.jpg",
    "https://cdn.pixabay.com/photo/2016/11/22/07/09/spruce-1848543__340.jpg"
  ];
  List<Map> categoryList = [
    {'img': 'assets/images/responsive.png', 'title': 'Electronics'},
    {'img': 'assets/images/necklace.png', 'title': 'Jewelery'},
    {'img': 'assets/images/fashion.png', 'title': "Men's clothing"},
    {'img': 'assets/images/dress.png', 'title': "Women's clothing"},
    {'img': 'assets/images/responsive.png', 'title': 'Electronics'},
    {'img': 'assets/images/necklace.png', 'title': 'Jewelery'},
    {'img': 'assets/images/fashion.png', 'title': "Men's clothing"},
    {'img': 'assets/images/dress.png', 'title': "Women's clothing"},
  ];

  @override
  void initState() {
    super.initState();
    controller = PageController(
      viewportFraction: 0.9,
      initialPage: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: appBar(),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
          context,
          CustomScrollView(
            physics: const BouncingScrollPhysics(),
            slivers: [
              SliverAppBar(
                elevation: 1.0,
                actions: [Expanded(child: _buildSearchField())],
                backgroundColor: Colors.white,
                floating: true,
                expandedHeight: SizeConfig.heightMultiplier * 7,
              ),
              SliverList(
                delegate: SliverChildListDelegate([
                  Util.height(10),
                  userView(),
                  imageSlider(),
                  Util.height(10),
                  category(),
                  Util.height(10),
                  todayDeals('Deals of the day'),
                  Util.height(20),
                  todayDeals('Special For You'),
                  Util.height(15),
                  popularItems(),
                ]),
              ),
            ],
          ),
        ));
  }

  Widget imageSlider() {
    return CarouselSlider(
      options: CarouselOptions(
        autoPlay: true,
        aspectRatio: 2.0,
        enlargeCenterPage: true,
      ),
      items: imageList
          .map((item) => SizedBox(
                height: 150,
                child: Center(
                    child: ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: Image.network(
                    item,
                    width: double.infinity,
                    fit: BoxFit.cover,
                    height: SizeConfig.heightMultiplier * 20,
                  ),
                )),
              ))
          .toList(),
    );
  }

  Widget _buildSearchField() {
    return Row(
      children: [
        Util.width(SizeConfig.widthMultiplier * 4),
        Expanded(child: _buildSearchField1()),
        Util.width(SizeConfig.widthMultiplier * 5),
        GestureDetector(
          onTap: () {
            Util.openActivity(context, const FilterView());
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              color: const Color.fromRGBO(238, 242, 255, 1.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset(
                'assets/images/filter.png',
                color: Colors.black,
                height: 30,
                filterQuality: FilterQuality.high,
              ),
            ),
          ),
        ),
        Util.width(SizeConfig.widthMultiplier * 5),
      ],
    );
  }

  Widget category() {
    return Padding(
      padding: const EdgeInsets.only(left: 18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  'Explore by Category',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.textMultiplier * 2.6),
                ),
              ),
              TextButton(
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(7.0)),
                      ),
                      backgroundColor: MaterialStateProperty.all(
                          const Color.fromRGBO(238, 242, 255, 1.0))),
                  onPressed: () {},
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 3, vertical: 4),
                    child: Text(
                      'View All',
                      style: TextStyle(fontSize: 17, color: Colors.black),
                    ),
                  )),
              Util.width(SizeConfig.widthMultiplier * 3),
            ],
          ),
          Util.height(10),
          SizedBox(
            height: SizeConfig.heightMultiplier * 13,
            child: ListView.builder(
                physics: const BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemCount: categoryList.length,
                itemBuilder: (ctx, index) {
                  return Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: Card(
                      margin: EdgeInsets.zero,
                      elevation: 0,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Image.asset(
                              categoryList[index]['img'],
                              height: 60,
                              filterQuality: FilterQuality.high,
                            ),
                          ),
                          Text(
                            categoryList[index]['title'],
                            style: const TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w400),
                          )
                        ],
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  Widget todayDeals(title) {
    return Padding(
      padding: const EdgeInsets.only(left: 18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  title,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.textMultiplier * 2.6),
                ),
              ),
              TextButton(
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(7.0)),
                      ),
                      backgroundColor: MaterialStateProperty.all(
                          const Color.fromRGBO(238, 242, 255, 1.0))),
                  onPressed: () {},
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 3, vertical: 4),
                    child: Text(
                      'View All',
                      style: TextStyle(fontSize: 17, color: Colors.black),
                    ),
                  )),
              Util.width(SizeConfig.widthMultiplier * 3),
            ],
          ),
          Util.height(20),
          SizedBox(
            height: SizeConfig.heightMultiplier * 22,
            child: ListView.builder(
                physics: const BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemCount: categoryList.length,
                itemBuilder: (ctx, index) {
                  return GestureDetector(
                    onTap: () {
                      Util.openActivity(context, const ItemDetails());
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 15),
                      child: Card(
                        elevation: 0.0,
                        margin: EdgeInsets.zero,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ClipRRect(
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12)),
                              child: Image.network(
                                (title.toString().contains('Special'))
                                    ? 'https://cdn.vox-cdn.com/thumbor/S4ka2uwWyJ9rHJFDwVa8BQCqMHA=/1400x788/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/22406771/Exbfpl2WgAAQkl8_resized.jpeg'
                                    : 'https://media.istockphoto.com/photos/multicolored-shoe-on-gradient-background-3d-rendering-picture-id1172886559?k=20&m=1172886559&s=612x612&w=0&h=tDH4hBFP0sgK7Zi722UhUCawu14F5DrwqbfnQqxCEHo=',
                                height: SizeConfig.imageSizeMultiplier * 15,
                                filterQuality: FilterQuality.high,
                              ),
                            ),
                            Util.height(10),
                            SizedBox(
                              width: SizeConfig.widthMultiplier * 30,
                              child: const Text(
                                'Erek Performance Shoes BX-029',
                                style: TextStyle(
                                    fontWeight: FontWeight.w400, fontSize: 17),
                              ),
                            ),
                            Util.height(8),
                            SizedBox(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    'Rs.28,00',
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 19),
                                  ),
                                  Util.height(2),
                                  const Text(
                                    'Rs.3200000,00',
                                    style: TextStyle(
                                        decoration: TextDecoration.lineThrough,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 17),
                                  ),
                                ],
                              ),
                            ),
                            Util.height(5),
                            RatingBar.builder(
                              initialRating: 4,
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              unratedColor: Colors.grey.withOpacity(0.6),
                              itemCount: 5,
                              itemSize: 14.0,
                              itemPadding:
                                  const EdgeInsets.symmetric(horizontal: 1.0),
                              itemBuilder: (context, _) => const Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {},
                              updateOnDrag: true,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  Widget popularItems() {
    return Padding(
      padding: const EdgeInsets.only(left: 18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  'Popular Products',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.textMultiplier * 2.6),
                ),
              ),
              TextButton(
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(7.0)),
                      ),
                      backgroundColor: MaterialStateProperty.all(
                          const Color.fromRGBO(238, 242, 255, 1.0))),
                  onPressed: () {},
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 3, vertical: 4),
                    child: Text(
                      'View All',
                      style: TextStyle(fontSize: 17, color: Colors.black),
                    ),
                  )),
              Util.width(SizeConfig.widthMultiplier * 3),
            ],
          ),
          Util.height(20),
          SizedBox(
            height: SizeConfig.heightMultiplier * 120,
            child: GridView.count(
                physics: const NeverScrollableScrollPhysics(),
                crossAxisCount: 2,
                cacheExtent: 1200,
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 10.0,
                childAspectRatio: 0.75,
                children: List.generate(10, (index) {
                  return InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {},
                    child: Card(
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12)),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.widthMultiplier * 5.5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Stack(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(12.0),
                                  clipBehavior: Clip.antiAlias,
                                  child: Image.network(
                                    index % 3 == 0
                                        ? 'https://ae01.alicdn.com/kf/HTB1BME5kRUSMeJjy1zjq6A0dXXaT.jpg?size=119388&height=800&width=800&hash=989679a63cd1612b47a9ecd90d2df19f'
                                        : 'https://oldnavy.gapcanada.ca/Asset_Archive/ONWeb/content/0028/064/980/assets/211112_15-M6132_M_DP_Sale.jpg',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Positioned(
                                  right: 5.0,
                                  top: 0.0,
                                  child: Container(
                                    width: 35,
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.white),
                                    child: IconButton(
                                      splashColor: Colors.transparent,
                                      hoverColor: Colors.transparent,
                                      onPressed: () {},
                                      icon: Icon(
                                        index % 3 == 0
                                            ? Icons.favorite
                                            : Icons.favorite_border,
                                        size: 16,
                                        color: index % 3 == 0
                                            ? Colors.red
                                            : Colors.black,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              index % 3 == 0
                                  ? 'Women Cotton Long-sleeve'
                                  : 'Men Cotton Winter Collection',
                              textScaleFactor: 1.0,
                              maxLines: 2,
                              textAlign: TextAlign.start,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                  fontSize: 12,
                                  fontFamily: 'mb',
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400),
                            ),
                            Util.height(8),
                            SizedBox(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    'Rs.28,00',
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 19),
                                  ),
                                  Util.height(2),
                                  const Text(
                                    'Rs.3200000,00',
                                    style: TextStyle(
                                        decoration: TextDecoration.lineThrough,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 17),
                                  ),
                                ],
                              ),
                            ),
                            Util.height(5),
                            RatingBar.builder(
                              initialRating: 4,
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              unratedColor: Colors.grey.withOpacity(0.6),
                              itemCount: 5,
                              itemSize: 14.0,
                              itemPadding:
                                  const EdgeInsets.symmetric(horizontal: 1.0),
                              itemBuilder: (context, _) => const Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {},
                              updateOnDrag: true,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                })),
          ),
        ],
      ),
    );
  }

  Widget _buildSearchField1() {
    return GestureDetector(
      onTap: () {
        Util.openActivity(context, const SearchView());
      },
      child: AbsorbPointer(
        child: Container(
          decoration: BoxDecoration(
              color: const Color.fromRGBO(238, 242, 255, 1.0),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              Util.width(SizeConfig.widthMultiplier * 2.5),
              const Icon(
                Icons.search,
                color: Colors.black,
              ),
              Util.width(SizeConfig.widthMultiplier * 5),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(238, 242, 255, 1.0),
                      borderRadius: BorderRadius.circular(10)),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      hintText: "Search in Nepdeal",
                      border: InputBorder.none,
                      hintStyle: TextStyle(color: Colors.black, fontSize: 17),
                    ),
                    style: const TextStyle(color: Colors.white, fontSize: 16.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget appBar() {
    return AppBar(
      centerTitle: true,
      title: Text(
        'Nepdeal',
        style: TextStyle(
            color: Colors.black, fontSize: SizeConfig.textMultiplier * 2.7),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      leading: IconButton(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        icon: Image.asset(
          'assets/images/hamburger.png',
          filterQuality: FilterQuality.high,
          height: SizeConfig.imageSizeMultiplier * 7.0,
        ),
        onPressed: () {
          widget.drawerOpen();
        },
      ),
      actions: [
        Stack(
          children: [
            IconButton(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              icon: Icon(
                Icons.shopping_bag_outlined,
                color: Colors.black,
                size: SizeConfig.imageSizeMultiplier * 5.5,
              ),
              onPressed: () {
                Util.openActivity(context, const MyCartView());
              },
            ),
            Positioned(
              right: 5.0,
              top: 7.0,
              child: Container(
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.red,
                  ),
                  child: const Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Text(
                      '3',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                    ),
                  )),
            ),
          ],
        )
      ],
    );
  }

  void _openCustomDialog() {
    showGeneralDialog(
        barrierColor: Colors.transparent,
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
              opacity: a1.value,
              child: AlertDialog(
                title: const Text(
                  'Confirm Logout',
                  textScaleFactor: 1.0,
                ),
                content: const Text(
                  'Are you sure you want to logout?',
                  textScaleFactor: 1.0,
                ),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(
                      'Cancel',
                      textScaleFactor: 1.0,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  TextButton(
                    onPressed: () async {
                      Util.replaceActivity(context, const LoginView());
                    },
                    child: const Text(
                      'Yes',
                      textScaleFactor: 1.0,
                      style: TextStyle(color: Colors.red),
                    ),
                  )
                ],
              ),
            ),
          );
        },
        transitionDuration: const Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        });
  }

  Widget userInfo(Map user) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Hero(
            tag: 'pp',
            child: CircleAvatar(
              radius: 50.0,
              backgroundImage: NetworkImage(
                  'https://images.fineartamerica.com/images/artworkimages/mediumlarge/2/zoro-one-piece-anime-ihab-design.jpg'),
              backgroundColor: Colors.transparent,
            ),
          ),
          Expanded(
            child: ListTile(
              title: Text(
                'Prijam Thapa',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: SizeConfig.textMultiplier * 3.5,
                    color: Colors.black),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(
                  user['email'],
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.grey,
                      fontSize: SizeConfig.textMultiplier * 2.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget userView() {
    return ListTile(
      title: Text(
        'Hello Prijam',
        style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: SizeConfig.textMultiplier * 3.2,
            wordSpacing: 1.3,
            letterSpacing: 1.2),
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(top: 2),
        child: Text(
          'Lets get something?',
          style: TextStyle(
              color: Colors.grey,
              fontSize: SizeConfig.textMultiplier * 2.3,
              wordSpacing: 1.3,
              letterSpacing: 1.2),
        ),
      ),
    );
  }

// Widget bannerView() {
//   return PageView(
//       controller: controller,
//       pageSnapping: true,
//       clipBehavior: Clip.antiAlias,
//       onPageChanged: (index) {},
//       children: List.generate(
//         banner.length,
//         (index) {
//           return Padding(
//             padding: const EdgeInsets.only(right: 15.0, left: 0.0),
//             child: Card(elevation: 5,
//               margin: EdgeInsets.zero,
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(22)),
//               color: banner[index],
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 mainAxisSize: MainAxisSize.min,
//                 children: [
//                   Padding(
//                     padding: EdgeInsets.only(
//                         top: 28.0,
//                         right: SizeConfig.widthMultiplier * 15,
//                         left: 15),
//                     child: Text(
//                       '30% OFF DURING COVID 21',
//                       style: TextStyle(
//                           color: Colors.white,
//                           fontWeight: FontWeight.bold,
//                           fontSize: SizeConfig.textMultiplier * 4),
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(left:8.0,top: 15),
//                     child: Container(
//                       decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(18),
//                         color: Colors.white,
//                       ),
//                       child: Padding(
//                         padding: EdgeInsets.symmetric(horizontal: 15,vertical: 12),
//                         child: Text(
//                           'Get Now',
//                           style: TextStyle(color: banner[index],fontSize: 18),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           );
//         },
//       ));
// }
}

class DrawerClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0.0, size.height - 10);
    path.lineTo(size.width, size.height - 85);
    path.lineTo(size.width, 0.0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
