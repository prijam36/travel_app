import 'dart:async';

import 'package:e_com/anim_btn.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/styles.dart';
import 'package:e_com/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddAddress extends StatefulWidget {
  const AddAddress({Key key}) : super(key: key);

  @override
  _AddAddressState createState() => _AddAddressState();
}

class _AddAddressState extends State<AddAddress> {
  final AnimButtonController _animButtonController = AnimButtonController();

  void _doSomething() async {
    _animButtonController.start();
    Timer(const Duration(seconds: 1), () {
      _animButtonController.stop();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: AnimButton(
            title: 'Save',
            width: SizeConfig.widthMultiplier * 80,
            controller: _animButtonController,
            onPressed: _doSomething,
            color: Util.themeColor,
            borderRadius: 18.0,
            height: SizeConfig.heightMultiplier * 6.5,
            successColor: Colors.green,
            curve: Curves.easeIn,
          ),
        ),
        appBar: Util.appBarBlack(context, 'Add New Address'),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 5),
              child:
                  ListView(physics: const BouncingScrollPhysics(), children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Username',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.4)),
                    Util.height(10),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      child: TextFormField(
                        style:
                            TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                        keyboardType: TextInputType.text,
                        onChanged: (val) {},
                        cursorColor: AppTheme.courColor,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: AppTheme.textWhiteGrey,
                          hintStyle: const TextStyle(
                            fontSize: 18,
                          ),
                          hintText: 'Username',
                          enabledBorder: const OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(14))),
                          border: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          disabledBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Util.height(20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Phone Number',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.4)),
                    Util.height(10),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      child: TextFormField(
                        style:
                            TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                        keyboardType: TextInputType.number,
                        onChanged: (val) {},
                        cursorColor: AppTheme.courColor,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: AppTheme.textWhiteGrey,
                          hintStyle: const TextStyle(
                            fontSize: 18,
                          ),
                          hintText: 'Phone Number',
                          enabledBorder: const OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(14))),
                          border: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          disabledBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Util.height(20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Region',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.4)),
                    Util.height(10),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      child: TextFormField(
                        style:
                            TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                        keyboardType: TextInputType.text,
                        onChanged: (val) {},
                        cursorColor: AppTheme.courColor,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: AppTheme.textWhiteGrey,
                          hintStyle: const TextStyle(
                            fontSize: 18,
                          ),
                          hintText: 'Region',
                          enabledBorder: const OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(14))),
                          border: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          disabledBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Util.height(20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('City',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.4)),
                    Util.height(10),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      child: TextFormField(
                        style:
                            TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                        keyboardType: TextInputType.text,
                        onChanged: (val) {},
                        cursorColor: AppTheme.courColor,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: AppTheme.textWhiteGrey,
                          hintStyle: const TextStyle(
                            fontSize: 18,
                          ),
                          hintText: 'City',
                          enabledBorder: const OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(14))),
                          border: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          disabledBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Util.height(20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Area',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.4)),
                    Util.height(10),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      child: TextFormField(
                        style:
                            TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                        keyboardType: TextInputType.text,
                        onChanged: (val) {},
                        cursorColor: AppTheme.courColor,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: AppTheme.textWhiteGrey,
                          hintStyle: const TextStyle(
                            fontSize: 18,
                          ),
                          hintText: 'Area',
                          enabledBorder: const OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(14))),
                          border: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          disabledBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Util.height(20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Address',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.4)),
                    Util.height(10),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      child: TextFormField(
                        style:
                            TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                        keyboardType: TextInputType.text,
                        onChanged: (val) {},
                        cursorColor: AppTheme.courColor,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: AppTheme.textWhiteGrey,
                          hintStyle: const TextStyle(
                            fontSize: 18,
                          ),
                          hintText: 'Address',
                          enabledBorder: const OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(14))),
                          border: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          disabledBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Util.height(20),
                Text(
                  ' Select a label for effective delivery',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.textMultiplier * 2.3),
                ),
                Util.height(20),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 18),
                        decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.3),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.work_outline_rounded,
                              color: Colors.grey,
                            ),
                            Text(
                              '  OFFICE',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SizeConfig.textMultiplier * 2.0),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Util.width(20),
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 18),
                        decoration: BoxDecoration(
                          color: Colors.green.withOpacity(0.3),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.work_outline_rounded,
                              color: Colors.green,
                            ),
                            Text(
                              '  HOME',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SizeConfig.textMultiplier * 2.0),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Util.height(20),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal:0, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ListTile(
                            leading: Transform(
                              transform: Matrix4.translationValues(-10, 0, 0),
                              child: const Text(
                                'Make default shipping address',
                                textScaleFactor: 1.0,
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 17),
                              ),
                            ),
                            trailing: CupertinoSwitch(
                              value:false,
                              onChanged: (value) async {

                                setState(() {});
                              },
                            ),
                          ),
                          Util.height(5),
                          ListTile(
                            leading: Transform(
                              transform: Matrix4.translationValues(-10, 0, 0),
                              child: const Text(
                                'Make default billing address',
                                textScaleFactor: 1.0,
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 17),
                              ),
                            ),
                            trailing: CupertinoSwitch(
                              value: true,
                              onChanged: (value) async {

                                setState(() {});
                              },
                            ),
                          ),
                        ],
                      ),
                    ),

                  ]),
            )));
  }
}
