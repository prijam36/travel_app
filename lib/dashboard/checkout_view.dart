import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class CheckOut extends StatefulWidget {
  const CheckOut({Key key}) : super(key: key);

  @override
  _CheckOutState createState() => _CheckOutState();
}

class _CheckOutState extends State<CheckOut> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Util.appBarBlack(context, 'Checkout'),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            Padding(
                padding: const EdgeInsets.only(left: 25, right: 5),
                child: ListView(
                  children: [
                    Util.height(15),
                    Text(
                      'Shipping to',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig.textMultiplier * 3.0),
                    ),
                    Util.height(15),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(2, (index) {
                        return GestureDetector(
                          onTap: () {
                            selectedIndex = index;
                            setState(() {});
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(top:8.0),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0)),
                              elevation: selectedIndex == index ? 3.0 : 0.0,
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: SizeConfig.heightMultiplier * 3,
                                    horizontal: 10),
                                child: Row(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: selectedIndex == index
                                              ? Colors.black
                                              : Colors.white,
                                          border: Border.all(
                                              color: Colors.black)),
                                      child: const Padding(
                                        padding: EdgeInsets.all(6.0),
                                        child: Text(' '),
                                      ),
                                    ),
                                    Util.width(15),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            index == 1 ? 'Office' : 'Home',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize:
                                                    SizeConfig.textMultiplier *
                                                        2.5),
                                          ),
                                          Util.height(4),
                                          Text(
                                            'Dhapasi-6,Kathmandu',
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize:
                                                    SizeConfig.heightMultiplier *
                                                        1.9),
                                          ),
                                          Util.height(4),
                                          Text(
                                            '9834572167,9855058552',
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize:
                                                    SizeConfig.heightMultiplier *
                                                        1.9),
                                          ),
                                        ],
                                      ),
                                    ),
                                    IconButton(
                                        onPressed: () {},
                                        icon: Icon(
                                          Icons.edit,
                                          size: 22,
                                        ))
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                    ),
                    Util.height(20),
                    Text(
                      'Payment Method',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig.textMultiplier * 2.8),
                    ),
                  ],
                ))));
  }
}
