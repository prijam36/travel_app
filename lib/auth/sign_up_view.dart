import 'dart:async';

import 'package:e_com/anim_btn.dart';
import 'package:e_com/auth/validation_stream.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/styles.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class SignUpView extends StatefulWidget {
  const SignUpView({Key key}) : super(key: key);

  @override
  State<SignUpView> createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  final TextBloc _textBloc = TextBloc();
  bool _hidePassword = true;
  final AnimButtonController _animButtonController = AnimButtonController();
  final TextEditingController _userName = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();

  void _doSomething() async {
    _animButtonController.start();
    Timer(const Duration(seconds: 1), () {
      _animButtonController.stop();
    });
  }

  @override
  void dispose() {
    _textBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            SizedBox(
                width: double.infinity,
                height: double.infinity,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Hero(
                        tag: 'logo',
                        child: Container(
                          padding: const EdgeInsets.all(22),
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color.fromRGBO(247, 248, 251, 1.0)),
                          child: Image.asset(
                            'assets/images/smartphone.png',
                            color: Colors.red,
                            filterQuality: FilterQuality.high,
                            height: SizeConfig.imageSizeMultiplier * 15,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Signup in to Nepdeal',
                        style: AppTheme.header,
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(
                        height: 13,
                      ),
                      GestureDetector(
                        onTap: () {
                          Util.closeActivity(context);
                        },
                        child: Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: "Already have an account?",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w400,
                                    fontSize: SizeConfig.textMultiplier * 2.3),
                              ),
                              TextSpan(
                                text: ' Login',
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    color: Colors.red,
                                    fontSize: SizeConfig.textMultiplier * 2.3),
                              ),
                            ],
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const SizedBox(
                            width: 60,
                          ),
                          Container(
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color.fromRGBO(59, 89, 153, 1.0)),
                            child: IconButton(
                                onPressed: null,
                                icon: Image.asset(
                                  'assets/images/fi.png',
                                  color: Colors.white,
                                  height: SizeConfig.imageSizeMultiplier * 5.5,
                                  filterQuality: FilterQuality.high,
                                )),
                          ),
                          Container(
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color.fromRGBO(252, 54, 55, 1.0)),
                            child: IconButton(
                                onPressed: null,
                                icon: Image.asset(
                                  'assets/images/google-plus.png',
                                  color: Colors.white,
                                  height: SizeConfig.imageSizeMultiplier * 6.5,
                                  filterQuality: FilterQuality.high,
                                )),
                          ),
                          Container(
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle, color: Colors.black),
                            child: IconButton(
                                onPressed: null,
                                icon: Image.asset(
                                  'assets/images/ai.png',
                                  color: Colors.white,
                                  height: SizeConfig.imageSizeMultiplier * 5.5,
                                  filterQuality: FilterQuality.high,
                                )),
                          ),
                          const SizedBox(
                            width: 60,
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 3,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 18),
                              child: Container(
                                height: SizeConfig.heightMultiplier * 0.07,
                                decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.7)),
                              ),
                            ),
                          ),
                          Text(
                            ' Or ',
                            style: TextStyle(
                                color: Colors.grey,
                                fontSize: SizeConfig.textMultiplier * 2.7),
                          ),
                          Expanded(
                            flex: 3,
                            child: Padding(
                              padding: const EdgeInsets.only(right: 18),
                              child: Container(
                                height: SizeConfig.heightMultiplier * 0.07,
                                decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.7)),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 18, vertical: 20),
                        child: Form(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Username',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize:
                                              SizeConfig.textMultiplier * 2.4)),
                                  Util.height(10),
                                  Container(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 3),
                                    decoration: BoxDecoration(
                                      color: AppTheme.textWhiteGrey,
                                      borderRadius: BorderRadius.circular(14.0),
                                    ),
                                    child: TextFormField(
                                      controller: _userName,
                                      style: TextStyle(
                                          fontSize:
                                              SizeConfig.textMultiplier * 2),
                                      keyboardType: TextInputType.text,
                                      onChanged: (val) {},
                                      cursorColor: AppTheme.courColor,
                                      decoration: const InputDecoration(
                                        hintStyle: TextStyle(
                                          fontSize: 18,
                                        ),
                                        hintText: 'Username',
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              StreamBuilder(
                                  stream: _textBloc.textStream,
                                  builder: (context,
                                      AsyncSnapshot<String> emailStream) {
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Email',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize:
                                                    SizeConfig.textMultiplier *
                                                        2.4)),
                                        Util.height(10),
                                        Container(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 3),
                                          decoration: BoxDecoration(
                                            color: AppTheme.textWhiteGrey,
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                          ),
                                          child: TextFormField(
                                            controller: _email,
                                            style: TextStyle(
                                                fontSize:
                                                    SizeConfig.textMultiplier *
                                                        2),
                                            keyboardType:
                                                TextInputType.emailAddress,
                                            onChanged: (val) {
                                              setState(() {});
                                              _textBloc.updateText(val, val);
                                            },
                                            cursorColor: AppTheme.courColor,
                                            decoration: InputDecoration(
                                              suffixIcon: emailStream
                                                          .hasError ||
                                                      emailStream.hasData
                                                  ? Tooltip(
                                                      showDuration:
                                                          const Duration(
                                                              seconds: 2),
                                                      decoration: BoxDecoration(
                                                        color:
                                                            emailStream.hasError
                                                                ? Colors.red
                                                                : Colors.green,
                                                      ),
                                                      message: emailStream
                                                              .hasError
                                                          ? emailStream.error
                                                          : "Available",
                                                      child: Icon(
                                                          emailStream.hasError
                                                              ? Icons.error
                                                              : Icons
                                                                  .check_circle,
                                                          color: emailStream
                                                                  .hasError
                                                              ? Colors.red
                                                              : Colors.green),
                                                    )
                                                  : null,
                                              hintStyle: const TextStyle(
                                                fontSize: 18,
                                              ),
                                              hintText: 'Email',
                                              border: const OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  }),
                              const SizedBox(
                                height: 20,
                              ),
                              StreamBuilder(
                                  stream: _textBloc.passStream,
                                  builder: (context,
                                      AsyncSnapshot<String> passStream) {
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Password',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize:
                                                    SizeConfig.textMultiplier *
                                                        2.4)),
                                        Util.height(10),
                                        Container(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 3),
                                          decoration: BoxDecoration(
                                            color: AppTheme.textWhiteGrey,
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                          ),
                                          child: TextFormField(
                                            obscureText: _hidePassword,
                                            controller: _password,
                                            cursorColor: AppTheme.courColor,
                                            style: TextStyle(
                                                fontSize:
                                                    SizeConfig.textMultiplier *
                                                        2),
                                            decoration: InputDecoration(
                                              hintText: 'Password',
                                              hintStyle: const TextStyle(
                                                fontSize: 18,
                                              ),
                                              suffixIcon: IconButton(
                                                color: Colors.grey,
                                                splashRadius: 1,
                                                icon: Icon(
                                                  !_hidePassword
                                                      ? Icons
                                                          .visibility_outlined
                                                      : Icons
                                                          .visibility_off_outlined,
                                                  color: !_hidePassword
                                                      ? Colors.green
                                                      : Colors.grey,
                                                ),
                                                onPressed: () {
                                                  _hidePassword =
                                                      !_hidePassword;
                                                  setState(() {});
                                                },
                                              ),
                                              border: const OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  }),
                              const SizedBox(
                                height: 20,
                              ),
                              StreamBuilder(
                                  stream: _textBloc.passStream,
                                  builder: (context,
                                      AsyncSnapshot<String> passStream) {
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Confirm Password',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize:
                                                    SizeConfig.textMultiplier *
                                                        2.4)),
                                        Util.height(10),
                                        Container(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 3),
                                          decoration: BoxDecoration(
                                            color: AppTheme.textWhiteGrey,
                                            borderRadius:
                                                BorderRadius.circular(14.0),
                                          ),
                                          child: TextFormField(
                                            obscureText: _hidePassword,
                                            controller: _password,
                                            cursorColor: AppTheme.courColor,
                                            style: TextStyle(
                                                fontSize:
                                                    SizeConfig.textMultiplier *
                                                        2),
                                            decoration: InputDecoration(
                                              hintText: 'Confirm Password',
                                              hintStyle: const TextStyle(
                                                fontSize: 18,
                                              ),
                                              suffixIcon: IconButton(
                                                color: Colors.grey,
                                                splashRadius: 1,
                                                icon: Icon(
                                                  !_hidePassword
                                                      ? Icons
                                                          .visibility_outlined
                                                      : Icons
                                                          .visibility_off_outlined,
                                                  color: !_hidePassword
                                                      ? Colors.green
                                                      : Colors.grey,
                                                ),
                                                onPressed: () {
                                                  _hidePassword =
                                                      !_hidePassword;
                                                  setState(() {});
                                                },
                                              ),
                                              border: const OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  }),
                              Util.height(25),
                              AnimButton(
                                title: 'Signup',
                                width: SizeConfig.widthMultiplier * 80,
                                controller: _animButtonController,
                                onPressed: _doSomething,
                                color: Util.themeColor,
                                borderRadius: 18.0,
                                height: SizeConfig.heightMultiplier * 6.5,
                                successColor: Colors.green,
                                curve: Curves.easeIn,
                              ),
                              Util.height(25),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        SizeConfig.widthMultiplier * 10),
                                child: Text.rich(
                                  TextSpan(
                                    children: [
                                      TextSpan(
                                        text:
                                            'By connecting your account confirm that you agree with our ',
                                        style: TextStyle(
                                            color: Colors.grey.withOpacity(0.8),
                                            fontSize:
                                                SizeConfig.textMultiplier *
                                                    2.1),
                                      ),
                                      TextSpan(
                                        text: ' Term and Condition',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:
                                                SizeConfig.textMultiplier *
                                                    2.2),
                                      ),
                                    ],
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ))));
  }
}
