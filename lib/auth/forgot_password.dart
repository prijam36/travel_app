import 'dart:async';

import 'package:e_com/auth/check_email.dart';
import 'package:e_com/auth/validation_stream.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/styles.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';
import 'package:stacked_firebase_auth/stacked_firebase_auth.dart';

import '../anim_btn.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final TextEditingController _email = TextEditingController();
  final TextBloc _textBloc = TextBloc();
  final AnimButtonController _animButtonController = AnimButtonController();

  Future forgotPassword() async {
    if (_email.text.isEmpty) {
      Util().toast('Email cannot be empty', Colors.black);
    } else {
      try {
        _animButtonController.start();
        var result = await FirebaseAuthenticationService()
            .sendResetPasswordLink(_email.text);
        if (result == true) {
          Util.openActivity(context, const CheckEmailView(
            isProfile: false,
          ));
          _animButtonController.stop();
        } else {
          Util().toast(Util.errorMsg, Colors.black);
        }
      } catch (e) {
        _animButtonController.stop();
        Util().toast('Something went wrong', Colors.black);
      }
    }
  }

  @override
  void initState() {
    _textBloc.dispose();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: Util.appBarBlack(context, 'Forgot Password'),
      body: Util().safeArea(
          context,
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 15),
            child: ListView(
              children: [
                Util.height(2),
                const Text(
                  'Reset Password',
                  textScaleFactor: 1.0,
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                Util.height(8),
                Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: Text(
                    "Enter the email associated with your account and we'll send an email with instructions to reset your password.",
                    textScaleFactor: 1.0,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                        color: Colors.black.withOpacity(0.8)),
                  ),
                ),
                Util.height(50),

                StreamBuilder(
                    stream: _textBloc.textStream,
                    builder: (context, AsyncSnapshot<String> emailStream) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Email',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: SizeConfig.textMultiplier * 2.4)),
                          Util.height(10),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 3),
                            decoration: BoxDecoration(
                              color: AppTheme.textWhiteGrey,
                              borderRadius: BorderRadius.circular(14.0),
                            ),
                            child: TextFormField(
                              controller: _email,
                              style: TextStyle(
                                  fontSize: SizeConfig.textMultiplier * 2),
                              keyboardType: TextInputType.emailAddress,
                              onChanged: (val) {
                                setState(() {});
                                _textBloc.updateText(val, val);
                              },
                              cursorColor: AppTheme.courColor,
                              decoration: InputDecoration(
                                suffixIcon:
                                    emailStream.hasError || emailStream.hasData
                                        ? Tooltip(
                                            showDuration:
                                                const Duration(seconds: 2),
                                            decoration: BoxDecoration(
                                              color: emailStream.hasError
                                                  ? Colors.red
                                                  : Colors.green,
                                            ),
                                            message: emailStream.hasError
                                                ? emailStream.error
                                                : "Available",
                                            child: Icon(
                                                emailStream.hasError
                                                    ? Icons.error
                                                    : Icons.check_circle,
                                                color: emailStream.hasError
                                                    ? Colors.red
                                                    : Colors.green),
                                          )
                                        : null,
                                hintStyle: const TextStyle(
                                  fontSize: 18,
                                ),
                                hintText: 'Enter your email address',
                                border: const OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    }),

                Util.height(50),
                AnimButton(
                  title: 'Send Instructions',
                  controller: _animButtonController,
                  onPressed: forgotPassword,
                  color: Util.themeColor,
                  borderRadius: 12.0,
                  height: 58,
                  successColor: Colors.green,
                  curve: Curves.easeIn,
                ),
                // Util.buttonWidget(context, 'Send Instructions'),
              ],
            ),
          )),
    );
  }

  Widget emailCom() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(left: 5.0),
          child: Text(
            "Email address",
            textScaleFactor: 1.0,
            textAlign: TextAlign.justify,
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.w400),
          ),
        ),
        Util.height(8),
        StreamBuilder(
          stream: TextBloc().textStream,
          builder: (context, AsyncSnapshot<String> emailStream) {
            return TextFormField(
              cursorColor: Util.themeColor,
              keyboardType: TextInputType.emailAddress,
              controller: _email,
              onChanged: (val) {
                setState(() {});
                TextBloc().updateText(val, val);
              },
              style: const TextStyle(fontSize: 18.0, color: Colors.black),
              decoration: InputDecoration(
                suffixIcon: _email.value.text.isNotEmpty
                    ? Tooltip(
                        decoration: BoxDecoration(
                          color:
                              emailStream.hasError ? Colors.red : Colors.green,
                        ),
                        message: emailStream.hasError
                            ? emailStream.error
                            : "Available",
                        child: Icon(
                            emailStream.hasError
                                ? Icons.error
                                : Icons.check_circle,
                            color: emailStream.hasError
                                ? Colors.red
                                : Colors.green),
                      )
                    : null,
                hintText: 'Enter your email address',
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: const BorderSide(
                    color: Colors.grey,
                    width: 1.0,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                disabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                border: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.transparent),
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
            );
          },
        )
      ],
    );
  }
}
