import 'dart:async';

class TextBloc {
  var emailValid = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  final StreamController _textController = StreamController<String>.broadcast();
  final StreamController _passwordController =
      StreamController<String>.broadcast();

  Stream<String> get textStream => _textController.stream;

  Stream<String> get passStream => _passwordController.stream;

  updateText(String text, String email) {
    (!text.contains(emailValid))
        ? _textController.sink.addError("$email is not a valid email")
        : _textController.sink.add(text);
  }

  passCheck(String pass) {
    (pass.length <= 1) ?? _passwordController.sink.addError("");
  }

  dispose() {
    _textController.close();
    _passwordController.close();
  }
}
