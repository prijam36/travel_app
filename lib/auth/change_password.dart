import 'dart:async';
import 'package:e_com/anim_btn.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/styles.dart';
import 'package:e_com/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SetPassword extends StatefulWidget {
  const SetPassword({Key key}) : super(key: key);

  @override
  _SetPasswordState createState() => _SetPasswordState();
}

class _SetPasswordState extends State<SetPassword> {
  bool _hidePassword = true;
  final AnimButtonController _animButtonController = AnimButtonController();

  void _doSomething() async {
    _animButtonController.start();
    Timer(const Duration(seconds: 1), () {
      _animButtonController.stop();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Util.appBarBlack(context, 'Change Password'),
      body: Util().safeArea(
          context,
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 15),
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                Util.height(2),
                const Text(
                  'Create new password',
                  textScaleFactor: 1.0,
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
                Util.height(8),
                Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: Text(
                    "Your new password must be different from previous used passwords.",
                    textScaleFactor: 1.0,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.black.withOpacity(0.8)),
                  ),
                ),
                Util.height(40),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Old Password',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.4)),
                    Util.height(10),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                        color: AppTheme.textWhiteGrey,
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      child: TextFormField(
                        style:
                            TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        onChanged: (val) {},
                        cursorColor: AppTheme.courColor,
                        decoration: const InputDecoration(
                          hintStyle: TextStyle(
                            fontSize: 18,
                          ),
                          hintText: 'Enter your old password',
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Util.height(20),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('New Password',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.4)),
                    Util.height(10),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                        color: AppTheme.textWhiteGrey,
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      child: TextFormField(
                        style:
                            TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                        keyboardType: TextInputType.text,
                        onChanged: (val) {},
                        obscureText: true,

                        cursorColor: AppTheme.courColor,
                        decoration: const InputDecoration(
                          hintStyle: TextStyle(
                            fontSize: 18,
                          ),
                          hintText: 'Must be at least 8 characters',
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Util.height(20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Confirm Password',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.4)),
                    Util.height(10),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                        color: AppTheme.textWhiteGrey,
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      child: TextFormField(
                        style:
                            TextStyle(fontSize: SizeConfig.textMultiplier * 2),
                        keyboardType: TextInputType.text,
                        onChanged: (val) {},
                        obscureText: true,

                        cursorColor: AppTheme.courColor,
                        decoration: const InputDecoration(
                          hintStyle: TextStyle(
                            fontSize: 18,
                          ),
                          hintText: 'Both passwords must match',
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Util.height(40),

                AnimButton(
                  title: 'Change Password',
                  controller: _animButtonController,
                  onPressed: _doSomething,
                  color: Util.themeColor,
                  borderRadius: 12.0,
                  height: 58,
                  successColor: Colors.green,
                  curve: Curves.easeIn,
                )
              ],
            ),
          )),
    );
  }

  Widget passwordView(String title, helperText) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: Text(
            title,
            textScaleFactor: 1.0,
            textAlign: TextAlign.justify,
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.w400),
          ),
        ),
        Util.height(8),
        TextFormField(
          cursorColor: Util.themeColor,
          keyboardType: TextInputType.text,
          obscureText: _hidePassword,
          style: TextStyle(fontSize: 18.0, color: Colors.black),
          decoration: InputDecoration(
            suffixIcon: title == 'Password'
                ? IconButton(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    icon: _hidePassword
                        ? Icon(
                            Icons.visibility_off,
                            color: Colors.grey,
                          )
                        : Icon(
                            Icons.visibility,
                            color: Colors.green,
                          ),
                    onPressed: () {
                      setState(() {
                        _hidePassword = !_hidePassword;
                      });
                    },
                  )
                : null,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12.0),
              borderSide: BorderSide(
                color: Colors.grey,
                width: 1.0,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.circular(10.0),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.circular(10.0),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.circular(10.0),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.circular(10.0),
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        ),
        Util.height(8),
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: Text(
            helperText,
            textScaleFactor: 1.0,
            textAlign: TextAlign.justify,
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
          ),
        ),
      ],
    );
  }
}
