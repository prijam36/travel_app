import 'package:e_com/auth/login_view.dart';
import 'package:e_com/size_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import '../util.dart';

class CheckEmailView extends StatefulWidget {
  final bool isProfile;

  const CheckEmailView({Key key, this.isProfile = false}) : super(key: key);

  @override
  _CheckEmailViewState createState() => _CheckEmailViewState();
}

class _CheckEmailViewState extends State<CheckEmailView> {
  bool isProfile = false;

  @override
  void initState() {
    isProfile = widget.isProfile;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Util.appBarBlack(context, 'Check Mail'),
      body: Util().safeArea(
          context,
          SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Util.height(120),
                    Container(
                      decoration: BoxDecoration(
                          color: const Color.fromRGBO(247, 245, 255, 1.0),
                          borderRadius: BorderRadius.circular(20)),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 25, vertical: 15),
                        child: Image.asset(
                          'assets/images/open.png',
                          filterQuality: FilterQuality.high,
                          color: Util.themeColor,
                          height: SizeConfig.imageSizeMultiplier * 17,
                        ),
                      ),
                    ),
                    Util.height(25),
                    const Text(
                      'Check your mail',
                      textScaleFactor: 1.0,
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                    Util.height(10),
                    const Text(
                      "We have sent a password recover \n instructions to your email",
                      textScaleFactor: 1.0,
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.w400),
                    ),
                    Util.height(30),
                    TextButton(
                      child: const Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                        child: Text(
                          'Open email app',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 19),
                        ),
                      ),
                      onPressed: () async {},
                      style: TextButton.styleFrom(
                        backgroundColor: Util.themeColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                      ),
                    ),
                    Util.height(30),
                    TextButton(
                      onPressed: () {
                        isProfile
                            ? Util.closeActivity(context)
                            : Util.replaceActivity(context, const LoginView());
                      },
                      style: ButtonStyle(
                        overlayColor: MaterialStateColor.resolveWith(
                            (states) => Colors.transparent),
                      ),
                      child: const Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Text(
                          "Skip , I'll confirm later",
                          textScaleFactor: 1.0,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w400,
                              color: Colors.black),
                        ),
                      ),
                    )
                  ],
                )),
                Container(
                  child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Text(
                            "Did not receive the email? Check your spam filter,",
                            textScaleFactor: 1.0,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w400),
                          ),
                          Util.height(10),
                          Text.rich(
                            TextSpan(
                              children: [
                                const TextSpan(
                                  text: 'or  ',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500),
                                ),
                                TextSpan(
                                  text: isProfile
                                      ? 'try sending mail again'
                                      : 'try another email address',
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () => Util.closeActivity(context),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Util.themeColor),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Util.height(50),
              ],
            ),
          )),
    );
  }
}
