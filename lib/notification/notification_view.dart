import 'package:e_com/notification/notificaitoon_setting.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class NotificationView extends StatefulWidget {
  const NotificationView({Key key}) : super(key: key);

  @override
  _NotificationViewState createState() => _NotificationViewState();
}

class _NotificationViewState extends State<NotificationView> {
  List<Map> notificationList = [
    {'name': 'Zara', 'isSelected': false},
    {'name': 'Nike', 'isSelected': false},
    {'name': 'Erek', 'isSelected': false},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Util.appBarBlackAction(context, 'Notifications',
            const NotificationSetting(), Icons.settings),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            DefaultTabController(
              length: 4,
              child: Column(
                children: [
                  Util.height(10),
                  TabBar(
                      isScrollable: true,
                      unselectedLabelColor: Colors.black,
                      labelStyle: const TextStyle(color: Colors.white),
                      indicatorSize: TabBarIndicatorSize.label,
                      unselectedLabelStyle:
                          const TextStyle(color: Colors.black),
                      indicator: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.green),
                      tabs: [
                        Tab(
                          child: Container(
                            width: SizeConfig.widthMultiplier * 30,
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(
                                    color: Colors.green, width: 0.0)),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text(
                                "All",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        Tab(
                          child: Container(
                            width: SizeConfig.widthMultiplier * 30,
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(
                                    color: Colors.green, width: 0.0)),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text(
                                "Messages",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        Tab(
                          child: Container(
                            width: SizeConfig.widthMultiplier * 30,
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(
                                    color: Colors.green, width: 0.0)),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text(
                                "Delivery",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        Tab(
                          child: Container(
                            width: SizeConfig.widthMultiplier * 30,
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(
                                    color: Colors.green, width: 0.0)),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text(
                                "Discounts",
                                style: TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      ]),
                  Expanded(
                    child: TabBarView(children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Util.height(SizeConfig.heightMultiplier * 10),
                          Image.asset(
                            'assets/images/bellsnooze_120259.png',
                            filterQuality: FilterQuality.high,
                            height: SizeConfig.heightMultiplier * 25,
                          ),
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              "No Notifications Yet",
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                  fontWeight: FontWeight.w800, fontSize: 19),
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              "When you get notification,they'll show up here",
                              textAlign: TextAlign.center,
                              textScaleFactor: 1.0,
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                      ListView.builder(
                          itemCount: 15,
                          itemBuilder: (ctx, index) {
                            return Dismissible(
                              key: Key(index.toString()),
                              direction: DismissDirection.endToStart,
                              background: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 12),
                                child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(0),
                                      color: Util.themeColor,
                                    ),
                                    child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              right: 18.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: const [
                                              Icon(
                                                Icons.delete,
                                                size: 18,
                                                color: Colors.white,
                                              ),
                                              Text(
                                                'DELETE',
                                                textScaleFactor: 1.0,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 12,
                                                    fontFamily: 'mb',
                                                    color: Colors.white),
                                              )
                                            ],
                                          ),
                                        ))),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(12)),
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 9),
                                    child: ListTile(
                                      leading: Container(
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.green.withOpacity(0.6),
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                          child: const Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: Icon(
                                              Icons.notifications,
                                              color: Colors.white,
                                              size: 15,
                                            ),
                                          )),
                                      title: Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                          'Account notifications',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize:
                                                  SizeConfig.textMultiplier *
                                                      2.3),
                                        ),
                                      ),
                                      subtitle: Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                          'Here goes all the information about notification',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize:
                                                  SizeConfig.textMultiplier *
                                                      1.8),
                                        ),
                                      ),
                                      trailing: Text(
                                        '2021-02-08',
                                        style: TextStyle(
                                            fontSize:
                                                SizeConfig.textMultiplier *
                                                    1.5),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }),
                      ListView.builder(
                          itemCount: 15,
                          itemBuilder: (ctx, index) {
                            return Card();
                          }),
                      ListView.builder(
                          itemCount: 15,
                          itemBuilder: (ctx, index) {
                            return Card();
                          }),
                    ]),
                  ),
                ],
              ),
            )));
  }
}
