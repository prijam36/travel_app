import 'package:e_com/model/notification_model.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class NotificationSetting extends StatefulWidget {
  const NotificationSetting({Key key}) : super(key: key);

  @override
  _NotificationSettingState createState() => _NotificationSettingState();
}

class _NotificationSettingState extends State<NotificationSetting> {
  final Box userPref = Hive.box('userPref');

  NotificationPref notificationPref;

  void getNotificationPref() {
    Map data = userPref.get(Util.notificationKey);
    notificationPref = NotificationPref(
        push: data['push'],
        delivery: data['delivery'],
        msg: data['msg'],
        discount: data['discount']);
  }

  @override
  void initState() {
    getNotificationPref();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Util.appBarBlack(
          context,
          'Notification Setting',
        ),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            Padding(
              padding: const EdgeInsets.only(left: 25, right: 5),
              child: ListView(
                children: [
                  Util.height(SizeConfig.heightMultiplier * 1.5),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Show notifications',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: SizeConfig.textMultiplier * 2.7),
                      ),
                      Util.height(SizeConfig.heightMultiplier * 1.5),
                      Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          child: ListTile(
                            leading: const Text(
                              'Push Notifications',
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400, fontSize: 17),
                            ),
                            trailing: CupertinoSwitch(
                              value: notificationPref.push,
                              onChanged: (value) async {
                                notificationPref.push = value;
                                userPref.get(Util.notificationKey)['push'] =
                                    value;
                                setState(() {});
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Util.height(SizeConfig.heightMultiplier * 1.5),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Account notifications',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: SizeConfig.textMultiplier * 2.7),
                      ),
                      Util.height(SizeConfig.heightMultiplier * 1.5),
                      Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          child: Column(
                            children: [
                              ListTile(
                                leading: const Text(
                                  'Messages',
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 17),
                                ),
                                trailing: CupertinoSwitch(
                                  value: notificationPref.msg,
                                  onChanged: (value) async {
                                    notificationPref.msg = value;
                                    userPref.get(Util.notificationKey)['msg'] =
                                        value;
                                    setState(() {});
                                  },
                                ),
                              ),
                              Util.height(5),
                              ListTile(
                                leading: const Text(
                                  'Delivery Notifications',
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 17),
                                ),
                                trailing: CupertinoSwitch(
                                  value: notificationPref.delivery,
                                  onChanged: (value) async {
                                    notificationPref.delivery = value;
                                    userPref.get(
                                            Util.notificationKey)['delivery'] =
                                        value;
                                    setState(() {});
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Util.height(SizeConfig.heightMultiplier * 1.5),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Store notifications',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: SizeConfig.textMultiplier * 2.7),
                      ),
                      Util.height(SizeConfig.heightMultiplier * 1.5),
                      Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          child: ListTile(
                            leading: const Text(
                              'Discounts Notifications',
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400, fontSize: 17),
                            ),
                            trailing: CupertinoSwitch(
                              value: notificationPref.discount,
                              onChanged: (value) async {
                                notificationPref.discount = value;
                                userPref.get(Util.notificationKey)['discount'] =
                                    value;
                                setState(() {});
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )));
  }
}
