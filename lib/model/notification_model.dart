
class NotificationPref {
  bool push;
  bool discount;
  bool msg;
  bool delivery;

  NotificationPref({this.push, this.discount, this.msg, this.delivery});

  NotificationPref.fromJson(Map<String, dynamic> json) {
    push = json['push'];
    discount = json['discount'];
    msg = json['msg'];
    delivery = json['delivery'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['push'] = push;
    data['discount'] = discount;
    data['msg'] = msg;
    data['delivery'] = delivery;
    return data;
  }
}
