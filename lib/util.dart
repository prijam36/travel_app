import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:url_launcher/url_launcher.dart';

class Util {
  final String userPref = 'userPref';
  static String notificationKey = 'notificationKey';
  static String userKey = 'loginDetails';
  static String errorMsg = 'Something went wrong';
  static const TextStyle headerStyle =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 28, color: Colors.black);
  static const TextStyle subHeader = TextStyle(
      fontWeight: FontWeight.w400, fontSize: 18.5, color: Colors.grey);
  static Color themeColor = Colors.red[700];

  static Widget height(height) {
    return SizedBox(
      height: double.parse(height.toString()),
    );
  }

  static Widget width(width) {
    return SizedBox(
      width: double.parse(width.toString()),
    );
  }

  toast(message, Color color) {
    return Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.SNACKBAR,
        timeInSecForIosWeb: 1,
        backgroundColor: color,
        textColor: Colors.white,
        fontSize: 15.0);
  }

  static Widget appBarBlack(BuildContext context, String text) {
    return AppBar(
      backgroundColor: Colors.white,
      leading: IconButton(
        onPressed: () {
          closeActivity(context);
        },
        icon: Container(
          decoration:
              const BoxDecoration(color: Colors.black, shape: BoxShape.circle),
          child: const Padding(
            padding: EdgeInsets.all(4.0),
            child: Icon(
              Icons.keyboard_arrow_left_sharp,
              color: Colors.white,
              size: 25,
            ),
          ),
        ),
      ),
      title: Text(
        text,
        textScaleFactor: 1.0,
        style: const TextStyle(
            fontWeight: FontWeight.w700,
            color: Colors.black,
            fontSize: 17,
            fontFamily: 'mb'),
      ),
      centerTitle: true,
      elevation: 0.0,
    );
  }

  static Widget appBarNoLeading(BuildContext context, String text) {
    return AppBar(
      backgroundColor: Colors.white,
      leading: const SizedBox(),
      title: Text(
        text,
        textScaleFactor: 1.0,
        style: const TextStyle(
            fontWeight: FontWeight.w700,
            color: Colors.black,
            fontSize: 17,
            fontFamily: 'mb'),
      ),
      centerTitle: true,
      elevation: 0.0,
    );
  }

  static Widget appBarBlackAction(
      BuildContext context, String text, dynamic pageRoute, IconData iconData) {
    return AppBar(
      backgroundColor: Colors.white,
      leading: IconButton(
        onPressed: () {
          closeActivity(context);
        },
        icon: Container(
          decoration:
              const BoxDecoration(color: Colors.black, shape: BoxShape.circle),
          child: const Padding(
            padding: EdgeInsets.all(4.0),
            child: Icon(
              Icons.keyboard_arrow_left_sharp,
              color: Colors.white,
              size: 25,
            ),
          ),
        ),
      ),
      title: Text(
        text,
        textScaleFactor: 1.0,
        style: const TextStyle(
            fontWeight: FontWeight.w700,
            color: Colors.black,
            fontSize: 17,
            fontFamily: 'mb'),
      ),
      actions: [
        IconButton(
            onPressed: () {
              Util.openActivity(context, pageRoute);
            },
            icon: Icon(
              iconData,
              color: Colors.black,
            )),
      ],
      centerTitle: true,
      elevation: 0.0,
    );
  }

  static Widget buttonWidget(BuildContext context, String text) {
    return SizedBox(
        height: 60,
        width: double.infinity,
        child: TextButton(
          child: Text(
            text,
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w700, fontSize: 19),
          ),
          onPressed: () async {},
          style: ButtonStyle(
            backgroundColor:
                MaterialStateColor.resolveWith((states) => Util.themeColor),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            )),
            overlayColor:
                MaterialStateColor.resolveWith((states) => Colors.transparent),
          ),
        ));
  }

  static dynamic decodeJson(String jsonString) {
    return json.decode(jsonString);
  }

  static dynamic encodeJson(Object value) {
    return json.encode(value);
  }

  static void closeActivity(context) {
    Navigator.pop(context);
  }

  static Future<void> openActivity(context, object) async {
    return await Navigator.push(
      context,
      CupertinoPageRoute(builder: (context) => object),
    );
  }

  static void replaceActivity(context, object) {
    Navigator.pushAndRemoveUntil(context,
        CupertinoPageRoute(builder: (context) => object), (r) => false);
  }

  launchURL(String link) async {
    if (await canLaunch(link)) {
      await launch(link);
    } else {
      toast('Something went wrong', Colors.black);
      throw 'Could not launch $link';
    }
  }

  static Widget btn(String title, Color color, context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 18),
        child: Text(
          title,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 15),
          textScaleFactor: 1.0,
          textAlign: TextAlign.center,
        ),
      ),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0),
          color: color,
          border: Border.all(color: Colors.white)),
    );
  }

  Widget safeArea(BuildContext context, Widget child) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: Colors.white,
          statusBarIconBrightness: Brightness.dark,
        ),
        child: SafeArea(
            top: true,
            child: Container(
              child: child,
              color: Colors.white,
            )));
  }

  Future boxes() async {
    await Hive.openBox(userPref);
  }

  String convertMonth(int monthValue) {
    String currentDate;
    if (monthValue == 1) {
      currentDate = 'January';
    } else if (monthValue == 2) {
      currentDate = 'January';
    } else if (monthValue == 3) {
      currentDate = 'March';
    } else if (monthValue == 4) {
      currentDate = 'April';
    } else if (monthValue == 5) {
      currentDate = 'May';
    } else if (monthValue == 6) {
      currentDate = 'June';
    } else if (monthValue == 7) {
      currentDate = 'July';
    } else if (monthValue == 8) {
      currentDate = 'August';
    } else if (monthValue == 9) {
      currentDate = 'September';
    } else if (monthValue == 10) {
      currentDate = 'October';
    } else if (monthValue == 11) {
      currentDate = 'November';
    } else {
      currentDate = 'December';
    }

    return currentDate;
  }

  Widget progressBar() {
    return Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
      ),
    );
  }

  Widget safeAreaP(BuildContext context, Widget child) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark.copyWith(
            statusBarIconBrightness: Brightness.light,
            statusBarColor: themeColor),
        child: SafeArea(
            child: Container(
          child: child,
          color: Colors.white,
        )));
  }
}
