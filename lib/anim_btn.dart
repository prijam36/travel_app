import 'package:e_com/size_config.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

enum LoadingState { idle, loading, success, error }

class AnimButton extends StatefulWidget {
  final AnimButtonController controller;

  final VoidCallback onPressed;

  final String title;

  final Widget child;

  final Color color;

  final double height;

  final double width;

  final double loaderSize;

  final double loaderStrokeWidth;

  final bool animateOnTap;

  final Color valueColor;

  final Curve curve;

  final double borderRadius;

  final Duration duration;

  final double elevation;

  final Color errorColor;

  final Color successColor;

  final Color disabledColor;

  Duration get _borderDuration {
    return Duration(milliseconds: (duration.inMilliseconds / 2).round());
  }

  AnimButton(
      {Key key,
      this.controller,
      this.title,
      this.onPressed,
      this.child,
      this.color,
      this.height = 50,
      this.width = 300,
      this.loaderSize = 24.0,
      this.loaderStrokeWidth = 2.0,
      this.animateOnTap = true,
      this.valueColor = Colors.white,
      this.borderRadius = 18,
      this.elevation = 2,
      this.duration = const Duration(milliseconds: 500),
      this.curve = Curves.easeInOutCirc,
      this.errorColor = Colors.red,
      this.successColor,
      this.disabledColor});

  @override
  State<StatefulWidget> createState() => RoundedLoadingButtonState();
}

class RoundedLoadingButtonState extends State<AnimButton>
    with TickerProviderStateMixin {
  AnimationController _buttonController;
  AnimationController _borderController;
  AnimationController _checkButtonController;

  Animation _squeezeAnimation;
  Animation _bounceAnimation;
  Animation _borderAnimation;

  final _state = BehaviorSubject<LoadingState>.seeded(LoadingState.idle);
  bool show = true;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    var _check = Container(
        alignment: FractionalOffset.center,
        decoration: BoxDecoration(
          color: widget.successColor ?? theme.primaryColor,
          borderRadius:
              BorderRadius.all(Radius.circular(_bounceAnimation.value / 2)),
        ),
        width: _bounceAnimation.value,
        height: _bounceAnimation.value,
        child: _bounceAnimation.value > 20
            ? Icon(
                Icons.check,
                color: widget.valueColor,
              )
            : null);

    var _cross = Container(
        alignment: FractionalOffset.center,
        decoration: BoxDecoration(
          color: widget.errorColor,
          borderRadius:
              BorderRadius.all(Radius.circular(_bounceAnimation.value / 2)),
        ),
        width: _bounceAnimation.value,
        height: _bounceAnimation.value,
        child: _bounceAnimation.value > 20
            ? Icon(
                Icons.close,
                color: widget.valueColor,
              )
            : null);

    var _loader = SizedBox(
        height: widget.loaderSize,
        width: widget.loaderSize,
        child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(widget.valueColor),
            strokeWidth: widget.loaderStrokeWidth));

    var childStream = StreamBuilder(
      stream: _state,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data == LoadingState.loading) {
          show = false;
        } else {
          Future.delayed(const Duration(milliseconds: 300), () {
            setState(() {
              show = true;
            });
          });
        }
        return AnimatedSwitcher(
            duration: const Duration(milliseconds: 200),
            child: snapshot.data == LoadingState.loading
                ? _loader
                : AnimatedOpacity(
                    opacity: !show ? 0.0 : 1.0,
                    curve: Curves.easeIn,
                    duration: const Duration(milliseconds: 500),
                    child: Text(
                      show ? widget.title : '',
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 19),
                    ),
                  ));
      },
    );

    var _btn = ButtonTheme(
        shape: RoundedRectangleBorder(borderRadius: _borderAnimation.value),
        minWidth: _squeezeAnimation.value,
        height: SizeConfig.heightMultiplier * 6.5,
        // ignore: deprecated_member_use
        child: RaisedButton(
            padding: const EdgeInsets.all(0),
            child: childStream,
            color: widget.color,
            disabledColor: widget.disabledColor,
            elevation: widget.elevation,
            onPressed: widget.onPressed == null ? null : _btnPressed));

    return Container(
        height: SizeConfig.heightMultiplier * 6.5,
        child: Center(
            child: _state.value == LoadingState.error
                ? _cross
                : _state.value == LoadingState.success
                    ? _check
                    : _btn));
  }

  @override
  void initState() {
    super.initState();

    _buttonController =
        AnimationController(duration: widget.duration, vsync: this);

    _checkButtonController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);

    _borderController =
        AnimationController(duration: widget._borderDuration, vsync: this);

    _bounceAnimation =
        Tween<double>(begin: 0, end: SizeConfig.heightMultiplier * 6.5).animate(
            CurvedAnimation(
                parent: _checkButtonController, curve: Curves.elasticOut));
    _bounceAnimation.addListener(() {
      setState(() {});
    });

    _squeezeAnimation = Tween<double>(
            begin: SizeConfig.widthMultiplier * 83,
            end: SizeConfig.heightMultiplier * 6.5)
        .animate(
            CurvedAnimation(parent: _buttonController, curve: widget.curve));

    _squeezeAnimation.addListener(() {
      setState(() {});
    });

    _squeezeAnimation.addStatusListener((state) {
      if (state == AnimationStatus.completed && widget.animateOnTap) {
        widget.onPressed();
      }
    });

    _borderAnimation = BorderRadiusTween(
            begin: BorderRadius.circular(widget.borderRadius),
            end: BorderRadius.circular(SizeConfig.heightMultiplier * 6.5))
        .animate(_borderController);

    _borderAnimation.addListener(() {
      setState(() {});
    });

    widget.controller?._addListeners(_start, _stop, _success, _error, _reset);
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void dispose() {
    _buttonController.dispose();
    _checkButtonController.dispose();
    _borderController.dispose();
    _state.close();
    super.dispose();
  }

  _btnPressed() async {
    if (widget.animateOnTap) {
      _start();
    } else {
      widget.onPressed();
    }
  }

  _start() {
    _state.sink.add(LoadingState.loading);
    _borderController.forward();
    _buttonController.forward();
  }

  _stop() {
    _state.sink.add(LoadingState.idle);
    _buttonController.reverse();
    _borderController.reverse();
  }

  _success() {
    _state.sink.add(LoadingState.success);
    _checkButtonController.forward();
  }

  _error() {
    _state.sink.add(LoadingState.error);
    _checkButtonController.forward();
  }

  _reset() {
    _state.sink.add(LoadingState.idle);
    _buttonController.reverse();
    _borderController.reverse();
    _checkButtonController.reset();
  }
}

class AnimButtonController {
  VoidCallback _startListener;
  VoidCallback _stopListener;
  VoidCallback _successListener;
  VoidCallback _errorListener;
  VoidCallback _resetListener;

  _addListeners(
      VoidCallback startListener,
      VoidCallback stopListener,
      VoidCallback successListener,
      VoidCallback errorListener,
      VoidCallback resetListener) {
    this._startListener = startListener;
    this._stopListener = stopListener;
    this._successListener = successListener;
    this._errorListener = errorListener;
    this._resetListener = resetListener;
  }

  start() {
    _startListener();
  }

  stop() {
    _stopListener();
  }

  success() {
    _successListener();
  }

  error() {
    _errorListener();
  }

  reset() {
    _resetListener();
  }
}
