import 'package:e_com/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';

class UserService extends ChangeNotifier {
  final Box userPref = Hive.box('userPref');
  Map userInfo;


  void saveData(Map data) {
    userPref.put(Util.userKey, data);
    userLogIn();
    notifyListeners();
  }

  Map userLogIn() {
    return userInfo = userPref.get(Util.userKey);
  }

  Map getUser() {
    return userLogIn();
  }
}
