import 'package:e_com/auth/change_password.dart';
import 'package:e_com/auth/check_email.dart';
import 'package:e_com/auth/login_view.dart';
import 'package:e_com/dashboard/address_view.dart';
import 'package:e_com/dashboard/wish_list.dart';
import 'package:e_com/profile/personal_data_view.dart';
import 'package:e_com/provider/user_auth.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';

class SettingView extends StatefulWidget {
  final bool isProfile;

  const SettingView({Key key, this.isProfile = false}) : super(key: key);

  @override
  _SettingViewState createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    final user = context.watch<UserService>().getUser();
    return Scaffold(
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Util.themeColor,
          icon: const Icon(Icons.exit_to_app),
          onPressed: () {
            _openCustomDialog();
          },
          label: const Text(
            'Logout',
          ),
        ),
        appBar: widget.isProfile
            ? Util.appBarBlack(context, 'Settings')
            : Util.appBarNoLeading(context, 'Settings'),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            Padding(
                padding: const EdgeInsets.only(left: 15, right: 10),
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  children: [
                    userInfo(user),
                    Util.height(SizeConfig.heightMultiplier * 3.5),
                    Padding(
                      padding: EdgeInsets.only(
                          left: SizeConfig.widthMultiplier * 2.0),
                      child: headerView('Account'),
                    ),
                    GestureDetector(
                        onTap: () {
                          Util.openActivity(context, const PersonalView());
                        },
                        child: helperWidget('Personal Information')),
                    GestureDetector(
                        onTap: () {
                          Util.openActivity(context, const AddressView());
                        },
                        child: helperWidget('Address Information')),
                    Padding(
                      padding: EdgeInsets.only(
                          left: SizeConfig.widthMultiplier * 2.0),
                      child: headerView('General'),
                    ),
                    GestureDetector(
                        onTap: () {
                          Util.openActivity(context, const WishListView(
                            isProfile: true,
                          ));
                        },
                        child: helperWidget('Wishlist')),
                    helperWidget('My Purchase'),
                    helperWidget('My Cancellation'),
                    helperWidget('My Returns'),
                    Padding(
                      padding: EdgeInsets.only(
                          left: SizeConfig.widthMultiplier * 2.0),
                      child: headerView('Security'),
                    ),
                    GestureDetector(
                        onTap: () {
                          Util.openActivity(
                              context,
                              const CheckEmailView(
                                isProfile: true,
                              ));
                        },
                        child: helperWidget('Reset Password ')),
                    GestureDetector(
                        onTap: () {
                          Util.openActivity(context, const SetPassword());
                        },
                        child: helperWidget('Change Password')),
                    Padding(
                      padding: EdgeInsets.only(
                          left: SizeConfig.widthMultiplier * 2.0),
                      child: headerView('Support'),
                    ),
                    helperWidget('Contact Us'),
                    helperWidget("FAQ's"),
                    helperWidget('Rate us on playstore'),
                    Util.height(15),
                    Text(
                      'App Version : ${_packageInfo.version}',
                      textAlign: TextAlign.center,
                      style:
                          const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    Util.height(20),
                  ],
                ))));
  }

  Widget userInfo(Map data) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0),
      child: Row(
        children: [
          const Hero(
            tag: 'pp',
            child: CircleAvatar(
              radius: 40.0,
              backgroundImage: NetworkImage(
                  'https://images.fineartamerica.com/images/artworkimages/mediumlarge/2/zoro-one-piece-anime-ihab-design.jpg'),
              backgroundColor: Colors.transparent,
            ),
          ),
          Expanded(
            child: ListTile(
              title: Text(
                'Prijam Thapa',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: SizeConfig.textMultiplier * 3.3),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(
                  data['email'],
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.grey,
                      fontSize: SizeConfig.textMultiplier * 2.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget headerView(String title) {
    return Container(
      color: const Color.fromRGBO(249, 249, 249, 1.0),
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Text(
          title,
          textScaleFactor: 1.0,
          style: const TextStyle(
              color: Color.fromRGBO(112, 112, 112, 1),
              fontFamily: 'mb',
              fontSize: 16,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget helperWidget(String title) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: ListTile(
        leading: Text(
          title,
          textScaleFactor: 1.0,
          style: const TextStyle(
              color: Color.fromRGBO(62, 62, 62, 1),
              fontSize: 16,
              fontFamily: 'mr',
              fontWeight: FontWeight.w400),
        ),
        trailing: const Padding(
          padding: EdgeInsets.only(bottom: 8.0),
          child: Icon(
            Icons.arrow_forward_ios,
            color: Colors.grey,
            size: 18,
          ),
        ),
      ),
    );
  }

  void _openCustomDialog() {
    showGeneralDialog(
        barrierColor: Colors.transparent,
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
              opacity: a1.value,
              child: AlertDialog(
                title: const Text(
                  'Confirm Logout',
                  textScaleFactor: 1.0,
                ),
                content: const Text(
                  'Are you sure you want to logout?',
                  textScaleFactor: 1.0,
                ),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(
                      'Cancel',
                      textScaleFactor: 1.0,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  TextButton(
                    onPressed: () async {
                      Util.replaceActivity(context, const LoginView());
                    },
                    child: const Text(
                      'Yes',
                      textScaleFactor: 1.0,
                      style: TextStyle(color: Colors.red),
                    ),
                  )
                ],
              ),
            ),
          );
        },
        transitionDuration: const Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        });
  }
}
