import 'dart:async';
import 'dart:io';

import 'package:e_com/anim_btn.dart';
import 'package:e_com/auth/validation_stream.dart';
import 'package:e_com/provider/user_auth.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/styles.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';

class PersonalView extends StatefulWidget {
  const PersonalView({Key key}) : super(key: key);

  @override
  _PersonalViewState createState() => _PersonalViewState();
}

class _PersonalViewState extends State<PersonalView> {
  final AnimButtonController _animButtonController = AnimButtonController();
  final TextBloc _textBloc = TextBloc();
  final TextEditingController _userName = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _dob = TextEditingController();
  List<PlatformFile> _paths;
  DateTime selectedDate = DateTime.now();
  String _directoryPath;
  final bool _multiPick = false;
  bool _isChangingImage = false;
  bool _isEdit = false;
  String _extension;
  final _scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();
  String _fileName;
  int _genderIndex = 0;
  final Box userPref = Hive.box('userPref');

  void _pickFiles(FileType fileType) async {
    try {
      _directoryPath = null;
      _paths = (await FilePicker.platform.pickFiles(
        type: fileType,
        allowMultiple: _multiPick,
        onFileLoading: (FilePickerStatus status) => print(status),
        allowedExtensions: (_extension?.isNotEmpty ?? false)
            ? _extension.replaceAll(' ', '').split(',')
            : null,
      ))
          ?.files;
    } on PlatformException catch (e) {
      _logException('Unsupported operation' + e.toString());
    } catch (e) {
      _logException(e.toString());
    }
    if (!mounted) return;
    if (_paths != null) {
      setState(() {
        _isChangingImage = true;
        _fileName =
            _paths != null ? _paths.map((e) => e.name).toString() : '...';
      });
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: ColorScheme.dark(
                onPrimary: Colors.white,
                primary: Colors.redAccent,
                surface: Util.themeColor,
                onSurface: Colors.white,
              ),
              focusColor: Colors.white,
            ),
            child: child,
          );
        },
        helpText: 'Expiry Date',
        initialDate: selectedDate,
        firstDate: DateTime(1900, 01, 01),
        lastDate: selectedDate);

    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        String month = Util().convertMonth(selectedDate.month);
        _dob.text = selectedDate.day.toString() +
            ' ' +
            month +
            ' ' +
            selectedDate.year.toString();
      });
    }
  }

  void _logException(String message) {
    _scaffoldMessengerKey.currentState?.hideCurrentSnackBar();
    _scaffoldMessengerKey.currentState?.showSnackBar(
      SnackBar(
        content: Text(message),
      ),
    );
  }

  void _doSomething() async {
    _animButtonController.start();
    Timer(const Duration(seconds: 1), () {
      _animButtonController.stop();
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _textBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // userPref.put(Util.loginDetails, {'email':'prijam'});

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Util.closeActivity(context);
            },
            icon: Container(
              decoration: const BoxDecoration(
                  color: Colors.black, shape: BoxShape.circle),
              child: const Padding(
                padding: EdgeInsets.all(4.0),
                child: Icon(
                  Icons.keyboard_arrow_left_sharp,
                  color: Colors.white,
                  size: 25,
                ),
              ),
            ),
          ),
          title: const Text(
            'Personal Data',
            textScaleFactor: 1.0,
            style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.black,
                fontSize: 17,
                fontFamily: 'mb'),
          ),
          actions: [
            IconButton(
                onPressed: () {
                  _isEdit = !_isEdit;
                  setState(() {});
                },
                icon: Icon(
                  Icons.edit,
                  color: _isEdit ? Colors.green : Colors.black,
                )),
            Util.width(15),
          ],
          centerTitle: true,
          elevation: 0.0,
        ),
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1.0),
        body: Util().safeArea(
            context,
            Padding(
                padding: const EdgeInsets.only(left: 15, right: 10),
                child:
                    ListView(physics: const BouncingScrollPhysics(), children: [
                  GestureDetector(
                      onTap: () {
                        // _pickFiles(FileType.image);
                        Provider.of<UserService>(context, listen: false)
                            .saveData({'email': 'prijam36@gmail.com'});
                      },
                      child: imageView()),
                  Util.height(SizeConfig.heightMultiplier * 5),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Username',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2.4)),
                      Util.height(10),
                      Container(
                        padding: const EdgeInsets.symmetric(vertical: 3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14.0),
                        ),
                        child: TextFormField(
                          controller: _userName,
                          readOnly: !_isEdit,
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2),
                          keyboardType: TextInputType.text,
                          onChanged: (val) {},
                          cursorColor: AppTheme.courColor,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: AppTheme.textWhiteGrey,
                            hintStyle: const TextStyle(
                              fontSize: 18,
                            ),
                            hintText: 'Username',
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: !_isEdit
                                        ? Colors.transparent
                                        : Colors.grey,
                                    width: 0.0),
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(14))),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: !_isEdit
                                      ? Colors.transparent
                                      : Colors.grey,
                                  width: 0.0),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(14)),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: !_isEdit
                                      ? Colors.transparent
                                      : Colors.grey,
                                  width: 0.0),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(14)),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: !_isEdit
                                      ? Colors.transparent
                                      : Colors.grey,
                                  width: 0.0),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(14)),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Util.height(SizeConfig.heightMultiplier * 2.3),
                  StreamBuilder(
                      stream: _textBloc.textStream,
                      builder: (context, AsyncSnapshot<String> emailStream) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Email',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: SizeConfig.textMultiplier * 2.4)),
                            Util.height(10),
                            Container(
                              padding: const EdgeInsets.symmetric(vertical: 3),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14.0),
                              ),
                              child: TextFormField(
                                controller: _email,
                                readOnly: !_isEdit,
                                style: TextStyle(
                                    fontSize: SizeConfig.textMultiplier * 2),
                                keyboardType: TextInputType.emailAddress,
                                onChanged: (val) {
                                  setState(() {});
                                  _textBloc.updateText(val, val);
                                },
                                cursorColor: AppTheme.courColor,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: AppTheme.textWhiteGrey,
                                  suffixIcon: emailStream.hasError ||
                                          emailStream.hasData
                                      ? Tooltip(
                                          showDuration:
                                              const Duration(seconds: 2),
                                          decoration: BoxDecoration(
                                            color: emailStream.hasError
                                                ? Colors.red
                                                : Colors.green,
                                          ),
                                          message: emailStream.hasError
                                              ? emailStream.error
                                              : "Available",
                                          child: Icon(
                                              emailStream.hasError
                                                  ? Icons.error
                                                  : Icons.check_circle,
                                              color: emailStream.hasError
                                                  ? Colors.red
                                                  : Colors.green),
                                        )
                                      : null,
                                  hintStyle: const TextStyle(
                                    fontSize: 18,
                                  ),
                                  hintText: 'Email',
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: !_isEdit
                                              ? Colors.transparent
                                              : Colors.grey,
                                          width: 0.0),
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(14))),
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: !_isEdit
                                            ? Colors.transparent
                                            : Colors.grey,
                                        width: 0.0),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(14)),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: !_isEdit
                                            ? Colors.transparent
                                            : Colors.grey,
                                        width: 0.0),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(14)),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: !_isEdit
                                            ? Colors.transparent
                                            : Colors.grey,
                                        width: 0.0),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(14)),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      }),
                  Util.height(SizeConfig.heightMultiplier * 2.5),
                  GestureDetector(
                    onTap: () {
                      _isEdit ? _selectDate(context) : null;
                    },
                    child: AbsorbPointer(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Date of Birth',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: SizeConfig.textMultiplier * 2.4)),
                          Util.height(10),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 3),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14.0),
                            ),
                            child: TextFormField(
                              controller: _dob,
                              readOnly: !_isEdit,
                              style: TextStyle(
                                  fontSize: SizeConfig.textMultiplier * 2),
                              keyboardType: TextInputType.text,
                              onChanged: (val) {},
                              cursorColor: AppTheme.courColor,
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: AppTheme.textWhiteGrey,
                                hintStyle: const TextStyle(
                                  fontSize: 18,
                                ),
                                hintText: 'Date of birth',
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: !_isEdit
                                            ? Colors.transparent
                                            : Colors.grey,
                                        width: 0.0),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(14))),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: !_isEdit
                                          ? Colors.transparent
                                          : Colors.grey,
                                      width: 0.0),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(14)),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: !_isEdit
                                          ? Colors.transparent
                                          : Colors.grey,
                                      width: 0.0),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(14)),
                                ),
                                disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: !_isEdit
                                          ? Colors.transparent
                                          : Colors.grey,
                                      width: 0.0),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(14)),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Util.height(SizeConfig.heightMultiplier * 2.5),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Gender',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: SizeConfig.textMultiplier * 2.4)),
                      Util.height(15),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () {
                                if (_isEdit) _genderIndex = 0;
                                setState(() {});
                              },
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: SizeConfig.widthMultiplier * 3),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: AppTheme.textWhiteGrey,
                                      borderRadius: BorderRadius.circular(14)),
                                  child: ListTile(
                                    leading: Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: _genderIndex == 0
                                              ? const Color.fromRGBO(
                                                  97, 123, 243, 1.0)
                                              : Colors.white,
                                          border: Border.all(
                                              color: Colors.white, width: 6)),
                                      child: const Padding(
                                        padding: EdgeInsets.all(6.0),
                                        child: Text(' '),
                                      ),
                                    ),
                                    title: Transform(
                                      transform:
                                          Matrix4.translationValues(-10, 0, 0),
                                      child: Text(
                                        'Male',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize:
                                                SizeConfig.textMultiplier *
                                                    2.4),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: () {
                                  if (_isEdit) _genderIndex = 1;
                                  setState(() {});
                                },
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal:
                                          SizeConfig.widthMultiplier * 3),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: AppTheme.textWhiteGrey,
                                        borderRadius:
                                            BorderRadius.circular(14)),
                                    child: ListTile(
                                      leading: Container(
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: _genderIndex == 1
                                                ? const Color.fromRGBO(
                                                    97, 123, 243, 1.0)
                                                : Colors.white,
                                            border: Border.all(
                                                color: Colors.white, width: 6)),
                                        child: const Padding(
                                          padding: EdgeInsets.all(6.0),
                                          child: Text(' '),
                                        ),
                                      ),
                                      title: Transform(
                                        transform: Matrix4.translationValues(
                                            -10, 0, 0),
                                        child: Text(
                                          'Female',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize:
                                                  SizeConfig.textMultiplier *
                                                      2.4),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ],
                  ),
                  Util.height(SizeConfig.heightMultiplier * 8),
                  AnimButton(
                    title: 'Save',
                    controller: _animButtonController,
                    onPressed: _doSomething,
                    color: Util.themeColor,
                    borderRadius: 12.0,
                    height: 58,
                    successColor: Colors.green,
                    curve: Curves.easeIn,
                  )
                ]))));
  }

  Widget imageView() {
    PlatformFile asset;
    if (_paths != null) {
      asset = _paths[0];
      _isChangingImage = false;
    }
    return Stack(
      children: [
        _paths == null
            ? _isChangingImage
                ? Util().progressBar()
                : Hero(
                    tag: 'pp',
                    child: Center(
                      child: Container(
                        width: SizeConfig.widthMultiplier * 35,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(16.0),
                          child: Image.network(
                              'https://images.fineartamerica.com/images/artworkimages/mediumlarge/2/zoro-one-piece-anime-ihab-design.jpg',
                              filterQuality: FilterQuality.high,
                              height: SizeConfig.imageSizeMultiplier * 35,
                              fit: BoxFit.fitWidth),
                        ),
                      ),
                    ),
                  )
            : _isChangingImage
                ? Util().progressBar()
                : Center(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(16.0),
                        child: Image.file(
                            File(
                              asset.path,
                            ),
                            height: SizeConfig.imageSizeMultiplier * 35,
                            width: SizeConfig.widthMultiplier * 35,
                            filterQuality: FilterQuality.high,
                            fit: BoxFit.fitWidth),
                      ),
                    ),
                  ),
        Positioned(
          bottom: -2,
          right: SizeConfig.widthMultiplier * 25,
          child: Center(
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  width: 4.5,
                  color: const Color.fromRGBO(246, 247, 254, 1.0),
                ),
                borderRadius: BorderRadius.circular(12.0),
                color: const Color.fromRGBO(216, 222, 253, 1.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  'assets/images/photo-camera.png',
                  height: SizeConfig.imageSizeMultiplier * 5.5,
                  color: const Color.fromRGBO(119, 141, 248, 1.0),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
