import 'package:e_com/auth/login_view.dart';
import 'package:e_com/boarding/view_content.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:flutter/material.dart';

class WelcomeContentWidget extends StatefulWidget {
  const WelcomeContentWidget({Key key}) : super(key: key);

  @override
  State<WelcomeContentWidget> createState() => _WelcomeContentWidgetState();
}

class _WelcomeContentWidgetState extends State<WelcomeContentWidget> {
  List<WelcomeModel> welcomeModel = <WelcomeModel>[];
  int slideIndex = 0;
  PageController controller;

  @override
  void initState() {
    super.initState();
    welcomeModel = getSlides();
    controller = PageController(
      viewportFraction: 1.0,
      initialPage: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:  Colors.white,
      body: Column(
        children: [
          SizedBox(
            height: SizeConfig.heightMultiplier * 3,
          ),
          Expanded(
            flex: 1,
            child: PageView(
              controller: controller,
              pageSnapping: true,
              clipBehavior: Clip.antiAlias,
              onPageChanged: (index) {
                setState(() {
                  slideIndex = index;
                });
              },
              children: [
                _viewHolder(welcomeModel[0].imageAssetPath,
                    welcomeModel[0].title, welcomeModel[0].desc, slideIndex),
                _viewHolder(welcomeModel[1].imageAssetPath,
                    welcomeModel[1].title, welcomeModel[1].desc, slideIndex),
                _viewHolder(welcomeModel[2].imageAssetPath,
                    welcomeModel[2].title, welcomeModel[2].desc, slideIndex),
              ],
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(
                    welcomeModel.length,
                        (index) => buildDot(index, context),
                  ),
                ),
              ),
              Expanded(
                child: SizedBox(
                  width: SizeConfig.heightMultiplier * 1,
                ),
              ),
              Card(
                margin: EdgeInsets.zero,
                elevation: 4,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(14),
                ),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14),
                      color: Util.themeColor),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: IconButton(
                      onPressed: () {
                        controller.nextPage(
                          duration: const Duration(milliseconds: 400),
                          curve: Curves.easeIn,
                        );
                        if (slideIndex == 2) {
                          Util.replaceActivity(context, const LoginView());
                        }
                      },
                      icon: const Icon(
                        Icons.arrow_forward_ios_rounded,
                        color: Colors.white,
                        size: 22,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: SizeConfig.heightMultiplier * 2,
              ),
            ],
          ),
          SizedBox(
            height: SizeConfig.heightMultiplier * 8,
          ),
        ],
      ),
    );
  }

  Widget _viewHolder(String imagePath, title, desc, int index) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Util.height(SizeConfig.heightMultiplier * 5),
        Container(
          decoration:
          const BoxDecoration(shape: BoxShape.circle, color: Colors.white),
          child: Padding(
            padding: EdgeInsets.all(10 * SizeConfig.heightMultiplier),
            child: Image.asset(
              imagePath,
              filterQuality: FilterQuality.high,
              height: SizeConfig.imageSizeMultiplier * 40,
              fit: BoxFit.fill,
              color: Util.themeColor,
            ),
          ),
        ),
        SizedBox(
          height: SizeConfig.heightMultiplier * 5,
        ),
        Padding(
          padding: EdgeInsets.only(
              top: 1 * SizeConfig.heightMultiplier,
              right: 2 * SizeConfig.heightMultiplier,
              left: 1.5 * SizeConfig.heightMultiplier,
              bottom: 1.0 * SizeConfig.heightMultiplier),
          child: Align(
            alignment: Alignment.bottomLeft,
            child: Text(
              title,
              style: const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
              textAlign: TextAlign.start,
            ),
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: Padding(
            padding: EdgeInsets.only(
                bottom: 2 * SizeConfig.heightMultiplier,
                left: 1.5 * SizeConfig.heightMultiplier,
                right: 8 * SizeConfig.heightMultiplier),
            child: Text(
              desc,
              textAlign: TextAlign.justify,
              style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w400,
                  color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPageIndicator(bool isCurrentPage) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 3.0),
      height: 5.0,
      width: 15.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: isCurrentPage ? Colors.black.withOpacity(0.6) : Colors.grey[300],
      ),
    );
  }

  Container buildDot(int index, BuildContext context) {
    return Container(
      height: 10,
      width: slideIndex == index ? 25 : 10,
      margin: const EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Util.themeColor,
      ),
    );
  }
}
