class WelcomeModel {
  String imageAssetPath;
  String title;
  String desc;

  WelcomeModel({this.imageAssetPath, this.title, this.desc});

  void setImageAssetPath(String getImageAssetPath) {
    imageAssetPath = getImageAssetPath;
  }

  void setTitle(String getTitle) {
    title = getTitle;
  }

  void setDesc(String getDesc) {
    desc = getDesc;
  }

  String getImageAssetPath() {
    return imageAssetPath;
  }

  String getTitle() {
    return title;
  }

  String getDesc() {
    return desc;
  }
}

List<WelcomeModel> getSlides() {
  List<WelcomeModel> slides = <WelcomeModel>[];
  WelcomeModel sliderModel = WelcomeModel();

  sliderModel.setTitle("Find Your Favourite Products ");
  sliderModel.setDesc("Find your favourite products anytime anywhere.");
  sliderModel.setImageAssetPath("assets/images/i1.png");
  slides.add(sliderModel);
  sliderModel = WelcomeModel();

  sliderModel.setTitle("Discover your favourite products");
  sliderModel.setDesc(
      "We make it simple to find the products you crave. Enter your address and let us do the rest.");
  sliderModel.setImageAssetPath("assets/images/i2.png");
  slides.add(sliderModel);
  sliderModel = WelcomeModel();

  sliderModel.setTitle("Pick Up or Delivery");
  sliderModel.setDesc(
      "We make your products delivery fast, simple and free - non matter if you order online or pay cash.");
  sliderModel.setImageAssetPath("assets/images/i3.png");
  slides.add(sliderModel);
  sliderModel = WelcomeModel();

  return slides;
}
