import 'package:e_com/auth/login_view.dart';
import 'package:e_com/boarding/welcome_screen.dart';
import 'package:e_com/dashboard/dash_view.dart';
import 'package:e_com/dashboard/pageholder.dart';
import 'package:e_com/provider/user_auth.dart';
import 'package:e_com/size_config.dart';
import 'package:e_com/util.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await Hive.initFlutter();
  await Util().boxes();
  runApp(const StartApp());
}

class StartApp extends StatefulWidget {
  const StartApp({Key key}) : super(key: key);

  @override
  State<StartApp> createState() => _StartAppState();
}

class _StartAppState extends State<StartApp> {
  final Box userPref = Hive.box('userPref');
  bool _isLogin = false;

  void checkPref() {
    if (!userPref.containsKey(Util.notificationKey)) {
      Map notificationModel = {
        'push': true,
        'discount': true,
        'msg': true,
        'delivery': true
      };
      userPref.put(Util.notificationKey, notificationModel);
      userPref.put('1', 'aa');
      setState(() {});
    }
    if (userPref.containsKey(Util.userKey)) {
      _isLogin = true;
      setState(() {});
    } else {
      _isLogin = false;
      setState(() {});
    }
  }

  @override
  void initState() {
    checkPref();
    super.initState();
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => BottomNavigationBarProvider()),
        ChangeNotifierProvider(create: (_) => UserService())
      ],
      child: LayoutBuilder(
        builder: (context, constraints) {
          return OrientationBuilder(
            builder: (context, orientation) {
              SizeConfig().init(constraints, orientation);
              return MaterialApp(
                debugShowCheckedModeBanner: false,
                title: 'E-commerce',
                home: !_isLogin ? const LoginView() : const PageHolder(),
              );
            },
          );
        },
      ),
    );
  }
}
